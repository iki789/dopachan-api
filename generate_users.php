<?php 

require_once __DIR__ . "../vendor/autoload.php";

use MemeVibe\User;
use MemeVibe\Post;
use MemeVibe\Files;

$faker = \Faker\Factory::create();
$faker->addProvider(new Faker\Provider\en_US\Address($faker));
$faker->addProvider(new Faker\Provider\DateTime($faker));
$faker->addProvider(new Faker\Provider\en_US\Text($faker));
$faker->addProvider(new Faker\Provider\Image($faker));

// User
/*
	id: number,
  name: string,
  display_name: string,
  email?: string,
  about: string,
  avatar: string,
  account_type: number,
  password?: string,
  new_password?: string,
  location?: {
    country?: string,
    city?: string
  },
  gender?: number,
  dob?: {
    day: number,
    month: number,
    year: number
  },
  fb_id?: string
*/
// Users
$fs = new Files();
//for($i=0; $i<50; $i++){
//	$user = [];
//	$user['name'] = $faker->firstName." ".$faker->lastName;
//	$user['display_name'] = $user['name'];
//	$user['password'] = $faker->text(16);
//	$user['email'] = $faker->email;
//	$user['about'] = $faker->text(200);
//	// Get Avatar
//	$avatar = $faker->image("C:\\wamp64\\tmp", 100,100, 'people');
//	$path = $fs->move($avatar);
//	$url = $fs->path_to_url($path);
// 	$user['avatar'] = $url;
//	$user['account_type'] = 0;
//	$user['location']['city'] = $faker->city;
//	$user['location']['country'] = $faker->country;
//	$user['gender'] = rand(0, 1);
//	$user['dob']['day'] = rand(1, 30);
//	$user['dob']['month'] = rand(1, 12);
//	$user['dob']['year'] = rand(1982, 2018);
//	
//	//$put = User::create(0,"", $user);
//}

// Post
/*
	id: string,
  caption: string,
  source: string,
  reacts: DpReactionsInterface[],
  comments: DpComment[],
  isVid?: number,
  thumbnail?: string
*/

$users = User::find_all();

for($i=0; $i< 1; $i++){
	$user = array_rand($users);
	
	$post = [];
	$post['user_id'] = $users[$user]->id;
	$post['caption'] = $faker->text(rand(20, 100));
	$source = $faker->image("C:\\wamp64\\tmp", 400,350);
	$path = $fs->move($source);
	$url = $fs->path_to_url($path);
	$post['source'] = $url;
	$react_ids = [];
	while(!in_array($id = rand(0,19), $react_ids) && count($react_ids) < rand(3,5)){
		$react_ids[] = $id;
	}

	$likes = [];
	foreach($react_ids as $react_id){
		$likes_count = rand(1,21);
		$random_user_keys = array_rand($users, $likes_count); 
		if(!empty($random_user_keys)){
			foreach($random_user_keys as $k){
				$likes[] = $users[$k]->id;
			}
		}else{
			$likes = [];
		}
		$post['reacts'][] = [
			'id'=> $react_id,
			'liked'=>rand(0,1),
			'likes'=>$likes
		];
	}
	Post::Create($post);

}

//echo "<pre>";
//$post = (new \MongoDB\BSON\ObjectId("5b1e2c41fc13ae4966000000"))->getTimestamp();
//print_r(date("d m Y",$post));
?>