<?php namespace MemeVibe;

require_once __DIR__ . '/../vendor/autoload.php';

use \Firebase\JWT\JWT as FireJWT;

class JWT{
	
	private const ERR_301 = "Token expired";
	private const ERR_302 = "Token invalid";
	private const ERR_303 = "Not a valid issuer";
	public $token;
	private $secret;
	private $issuers;
	
	public function __construct($token=""){
		if(empty($token)){
			$this->token = $this->get_bearer_header();
		}else{
			$this->token = $token;
		}
		$this->secret = md5(sha1("@2DoUYOukNowDUgaNDAIeWaemaBuda!4"));
		$this->issuers = ["https://dopachan/", "https://dopachan.com"];
		
		if($this->token){
			$this->validate();
		}
	}
	
	public static function encode($sub){
		
		if(is_array($sub)){ 
			$sub = json_encode($sub); 
		}
		$jwt = new self();
		$token =[
			"iss" => implode(", ", $jwt->issuers),
			"aud" => "http://localhost:4200",
			"sub" => $sub,
			"exp" => strtotime("+1 month"),
			"iat" => time()
		];

		return $jwt = FireJWT::encode($token, $jwt->secret);
	}
	
	public static function decode($token=""){
		// If token is empty then get from headers! KAMEHAMEHAA!
		$jwt = new self($token);
		return FireJWT::decode($jwt->token, $jwt->secret, ['HS256']);
	}
	
	private function validate(){
		$jwt = FireJWT::decode($this->token, $this->secret, array('HS256'));
		// Check if issuer is us
		if(!array_intersect(explode(", ",$jwt->iss), $this->issuers)){
			throw new \Exception(self::ERR_303);
			return false;
		}
		// Check if token is expired
		if($jwt->exp < time()){
			throw new \Exception(self::ERR_301);
			return false;
		}
		return true;
	}
	
	private function get_auth_header(){
		$headers = null;
		if (isset($_SERVER['Authorization'])) {
			$headers = trim($_SERVER["Authorization"]);
		}
		else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
			$headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
		} elseif (function_exists('apache_request_headers')) {
			$requestHeaders = apache_request_headers();
			// Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
			$requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
			//print_r($requestHeaders);
			if (isset($requestHeaders['Authorization'])) {
				$headers = trim($requestHeaders['Authorization']);
			}
		}
		return $headers;
	}

	private function get_bearer_header() {
		$headers = $this->get_auth_header();
		// HEADER: Get the access token from the header
		if (!empty($headers)) {
			if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
				$token = $matches[1];
				return $token;
			}
		}
		return false;
	}

	public function get_user(){
		$token_decoded = self::decode($this->token);
		if($token_decoded){
			return $user = json_decode($token_decoded->sub, true);
		}
	}
}


//$str = ["http://ggo.com", "http://gsa"];
//print_r(implode(",", $str));
//
//$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZG9wYWNoYW5cLyIsImF1ZCI6Imh0dHA6XC9cL2xvY2FsaG9zdDo0MjAwIiwic3ViIjoie1wiaWRcIjpcIjVhZTlkODc4NjkyNWEzMmIyODAwMzMwMFwiLFwiZmJfaWRcIjpcIjE4NjI1NDc5OTM4MTc3MTRcIixcImF2YXRhclwiOlwiaHR0cDpcXFwvXFxcL2xvY2FsaG9zdDo4MFxcXC9tZW1ldmliZVxcXC9maWxlc1xcXC8wXFxcL1JFemFDcFg1a1hPRWdLZC5qcGdcIixcImRpc3BsYXlfbmFtZVwiOlwiSWtobGFrIFNIYWlraFwiLFwiYWNjb3VudF90eXBlXCI6MX0iLCJleHAiOjE1MzA1NDU0NDcsImlhdCI6MTUyNTI3NTA0N30.RGSpN1MdZxeowgw-chIsA3qW1fXjFkZ54fyIPz0wBbM";
//
//$jwt = new Jwt();
//$jwt->decode($token);

//$jwt = JWT::encode(['id'=>1]);
//print_r($jwt);

?>