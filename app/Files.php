<?php namespace MemeVibe;

require_once __DIR__ . "/../vendor/autoload.php";

class Files {
	
	const DS = Config::DS;
	const PATH = Config::FILE_STORAGE_PATH;
	public $tmp_folder = Config::FILE_TMP_PATH;
	public $folder;
	private $image_ext = ['jpg', 'jpeg', 'png', 'gif'];
	private $vid_ext = ['mp4'];
	
	public function __construct(){
		$this->folder = $this->get_folder();
	}
	
	public function upload_to_tmp($file){
		if(!$this->validate($file)){ 
			throw new \Exception("Not a valid file");
		}
		if(!file_exists($file['tmp_name'])){
			throw new \Exception("File not found");
		}
		
		$filename = $this->generate_file_name() . "." . pathinfo($file['name'], PATHINFO_EXTENSION);
		if(rename($file['tmp_name'], $this->tmp_folder.self::DS.$filename)){
			$path = $this->tmp_folder.self::DS.$filename;
			chmod($path, 0755);
			return $path;
		}else{
			return false;
		}
	}
	
	public function move_from_tmp($filename){
		$ext = strtolower(pathinfo(basename($filename), PATHINFO_EXTENSION));
		$new_filename = pathinfo($filename, PATHINFO_FILENAME);
		
		if($folders = scandir($this->folder)){
			while(in_array($new_filename, $folders)){
				$new_filename = $this->generate_file_name().".$ext";
			}
		}
		
		// Compress image
		if(in_array($ext, $this->image_ext) && $ext != "gif"){
			// Main bug arises here!
			$filename = pathinfo(($this->compress_img($this->tmp_folder.self::DS.$filename)), PATHINFO_BASENAME);	
			// Since we convert pngs to jpg, change $ext
			$ext = "jpg";
		}
		
		// Compress vid
		if(in_array($ext, $this->vid_ext)){
			$this->compress_vid($this->tmp_folder.self::DS.$filename);
		}
		
		// Move folder
		if(rename($this->tmp_folder.self::DS.$filename, $this->folder.self::DS.$new_filename.".".$ext)){
			return $this->folder.self::DS.$new_filename.".".$ext;
		}else{
			return false;
		}
	}
	
	public function move($file_path){
		$ext = pathinfo($file_path, PATHINFO_EXTENSION);
		$new_filename = basename($file_path);
		if($folders = scandir($this->folder)){
			while(in_array($new_filename, $folders)){
				$new_filename = $this->generate_file_name().".$ext";
			}
		}
		if(rename($file_path, $this->folder.self::DS.$new_filename)){
			return $this->folder.self::DS.$new_filename;
		}else{
			return false;
		}
	}
	
	private function validate($file){
		$is_valid = false;
		$max_size = 1024*1024*1024; 
		$ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
		
		if(in_array($ext, $this->vid_ext) || in_array($ext, $this->image_ext)){
			$is_valid = true;
		}
		if($file['size'] > $max_size){
			$is_valid = false;
		}
		return $is_valid;
	}
	
	public function generate_file_name($type='alnum', $length=15){
		switch ($type) {
			case 'alnum':
				$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'alpha':
				$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			case 'hexdec':
				$pool = '0123456789abcdef';
				break;
			case 'numeric':
				$pool = '0123456789';
				break;
			case 'nozero':
				$pool = '123456789';
				break;
			case 'distinct':
				$pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
				break;
			default:
				$pool = (string) $type;
				break;
		}


		$crypto_rand_secure = function ($min, $max) {
			$range = $max - $min;
			if ( $range < 0 ) return $min; // not so random...
			$log    = log( $range, 2 );
			$bytes  = (int) ( $log / 8 ) + 1; // length in bytes
			$bits   = (int) $log + 1; // length in bits
			$filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
			do {
				$rnd = hexdec( bin2hex( openssl_random_pseudo_bytes( $bytes ) ) );
				$rnd = $rnd & $filter; // discard irrelevant bits
			} while ( $rnd >= $range );
			return $min + $rnd;
		};

		$token = "";
		$max   = strlen( $pool );
		for ( $i = 0; $i < $length; $i++ ) {
			$token .= $pool[$crypto_rand_secure( 0, $max )];
		}
	return $token;

	}
	
	public function get_folder(){
		$main_folder = self::PATH;
		if(is_dir($main_folder)){
			$folders = scandir($main_folder);
			$total_folders = count($folders);
			foreach($folders  as $k=>$f){
				if(is_numeric($f)){
					if(count(scandir($main_folder . self::DS . $f)) < 10000){
						$folder = $main_folder . self::DS . $f;
						break;
					}
				}
			}
			if(isset($folder) && is_numeric(basename($folder))){
				return $folder;
			}else{
				return $this->create_main_folder_structure();
			}
		}else{
			return $this->create_main_folder_structure();
		}
	}
	
	private function create_main_folder_structure(){
		$main_folder = self::PATH;
		if(mkdir($main_folder . self::DS . '0', 0655, true)){
			return $main_folder . self::DS . '0';
		}
	}
	
	public function path_to_url($path){
		
		if(strpos($path, Config::FILE_TMP_PATH) !== false){
			// is tmp path
			return str_replace(
				[Config::FILE_TMP_PATH, "/\\", "\\"],
				[Config::FILE_TMP_URL, "/", "/"], 
			$path);
		}elseif(strpos($path, Config::FILE_STORAGE_PATH) !== false){
			return str_replace(
				[Config::FILE_STORAGE_PATH, "/\\", "\\"], 
				[Config::FILE_STORAGE_URL, "/", "/"], 
			$path);
		}elseif(strpos($path, Config::REACTIONS_PATH) !== false){
			return str_replace(
				[Config::REACTIONS_PATH, "/\\", "\\"], 
				[Config::REACTIONS_URL, "/", "/"], 
			$path);
		}else{
			throw new \Exception("Failed parsing PATH to URL");
		}
	}

	public function url_to_path($url){
		if(strpos($url, Config::FILE_TMP_URL) !== false){
			// is tmp path
			return str_replace(
			  [Config::FILE_TMP_URL, "\\"],
			  [Config::FILE_TMP_PATH, '/'],
			$url);
		}elseif(strpos($url, Config::FILE_STORAGE_URL) !== false){
            return str_replace(
                [Config::FILE_STORAGE_URL, "\\"],
                [Config::FILE_STORAGE_PATH, "/"],
                $url);
        }elseif(strpos($url, Config::REACTIONS_PATH) !== false){
            return str_replace(
                [Config::FILE_STORAGE_URL, "\\"],
                [Config::FILE_STORAGE_PATH, "/"],
                $url);
        }else{
			throw new \Exception("Failed parsing URL to PATH");
		}
	}
	
	public function compress_img($image_path){
		
		$image_name = pathinfo($image_path, PATHINFO_FILENAME);
		$image_dir = pathinfo($image_path, PATHINFO_DIRNAME);
		$image_size = getimagesize($image_path);
		$new_width = $image_size[0];
		$new_height = $image_size[1];
		$max_width = 1280;
		
		$imagine = new \Imagine\Gd\Imagine();
		$image = $imagine->open($image_path);
		
		if($image_size[0] > $max_width){
			// resize image
			// Height ratio fomular: height/width*new_width
			$new_width = $max_width;
			$new_height = ($image_size[1]/$image_size[0])*$max_width;
			
			if(pathinfo($image_path, PATHINFO_EXTENSION) == "png"){
				$png_bg = new \Imagine\Image\Box($image_size[0], $image_size[1]);
				$png_bg = $imagine->create($png_bg);
			}
		}
		if(isset($png_bg)){
			$image->paste($png_bg,new \Imagine\Image\Point(0,0));	
		}
		$image->resize(new \Imagine\Image\Box($new_width, $new_height))
					->save($image_dir.self::DS.$image_name.".jpg");
		$image_path = $image_dir.self::DS.$image_name.".jpg";
		
		return $image_path;
		
	}
	
	public function compress_vid($video_path){
		$max_mbs = 8 * 1024 *1024;
		if(filesize($video_path) < $max_mbs){
			return true;
		}
		set_time_limit(0);
		
		/* Get Video details with ffprobe */
		$ffprobe = \FFMpeg\FFProbe::create();
		$ffprobe_stream = $ffprobe
		 ->streams($video_path)
		 ->videos()                   
		 ->first();

		$frames = $ffprobe_stream->get('nb_frames');
		$bit_rate = $ffprobe_stream->get('bit_rate');
		$bits_per_raw_sample = $ffprobe_stream->get('bits_per_raw_sample');
		$duration = $ffprobe_stream->get('duration');
		$width = $ffprobe_stream->get('width');
		$height = $ffprobe_stream->get('height');
		
		
		if($width < 720 && $height < 720 ){
			return true;
		}
		
		
		$path = pathinfo($video_path, PATHINFO_DIRNAME);
		$name = pathinfo($video_path, PATHINFO_FILENAME);
		$ext = pathinfo($video_path, PATHINFO_EXTENSION);
		$filesize = filesize($video_path);
		// rename with a temperory name
		$tmp_video_path = $path.self::DS.$name."this_is_tmp.".$ext;
		rename($video_path, $tmp_video_path);
		
		try{
			$ffmpeg = \FFMpeg\FFMpeg::create();
			$video = $ffmpeg->open($tmp_video_path);
			$format = new \FFMpeg\Format\Video\X264();
			$format
				->setKiloBitrate(1000)
				->setAudioChannels(2)
				->setAudioKiloBitrate(92)
				->setAudioCodec('aac');
			
			$video->save($format, $video_path);
			
			unlink($tmp_video_path);
			return true;
		}catch(\Alchemy\BinaryDriver\ConfigurationInterface $e){
			echo $e->getMessage();
		}catch(\Alchemy\BinaryDriver\Exception\ExecutionFailureException $e){
			echo $e->getMessage();
		}catch(\FFMpeg\Driver\FFProbeDriver $e){
			echo $e->getMessage();
		}
		
	}
	
	public function get_video_thumbnail($video_path){
		$tmp_thumb_path = $this->tmp_folder.self::DS.$this->generate_file_name().".jpg";
		$ffmpeg = \FFMpeg\FFMpeg::create();
		$video 	= $ffmpeg->open($video_path);
		$video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(1))
			->save($tmp_thumb_path);
		
		return $this->move_from_tmp(basename($tmp_thumb_path));
	}
}

//$path = "/home/iki789/public_html/dopachan/files/Je9u54Y0ynsFsLD.jpg";
//$url = "http://localhost:80/memevibe/files/0/KzeFj8AuyKaAY5Q.mp4";
//$fs = new Files();
//echo $fs->path_to_url($path);

?>