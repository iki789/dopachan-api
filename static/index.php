<?php

require_once __DIR__ . '/../vendor/autoload.php';
use \MemeVibe\Post as Post;
$id = isset($_GET['p']) && !empty($_GET['p']) ? $_GET['p'] : '' ;
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if(!empty($id)){
	$post = Post::find(['params'=>['_id'=>$id],'limit'=>1]);
}
if($post){$post = $post[0];}

$link = isset($post) &&  !empty($post) ? 'https://dopachan.com/post/'.$post->id : 'https://dopachan.com' ;
$title = ($_GET['t']) ?? $post->title ?? "Dopachan - Lets share it!";
$des = "Discover and share funny pics, videos, and memes found on the internet!";
$des =  isset($post) && !empty($post->title) ? "Dopachan - " . $des : $des ;

$link_image = 'https://f1.dopachan.com/files/0/38w9NgqhpEJHHfq.jpg';
if($post){
	if(!empty($post->isVid)){
		$link_image = $post->thumbnail ? $post->thumbnail : $link_image ; 
	}else{
		$link_image = $post->source ? $post->source : $link_image ;
	}
}
?>
<!doctype html>
<html>
<head>
	<title><?php echo $title ?></title>
<meta charset="utf-8">
  <meta name="application-name" content="<?php echo $title ?>"/>
  <meta name="msapplication-TileColor" content="#F1C40F" />
  <meta name="msapplication-TileImage" content="./mstile-144x144.png" />

  <!-- Open Graph -->
  <meta property="og:url" content="<?php echo $actual_link ?>">
  <meta property="og:image" content="<?php echo $link_image ?>">
  <meta property="og:description" content="<?php echo $des ?>">
  <meta property="og:title" content="<?php echo $title ?>">
  <meta property="og:site_name" content="Dopachan – Just share it!">
  <meta property="og:see_also" content="<?php echo $link ?>">

  <!-- Twitter -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:url" content="<?php echo $link ?>">
  <meta name="twitter:title" content="<?php echo $title ?>">
  <meta name="twitter:description" content="<?php echo $des ?>">
  <meta name="twitter:image" content="<?php echo $link_image ?>">

  <meta name="description" content="<?php echo $des ?>">
  <meta name="keywords" content="memes, meme, funny, hilarious, cats, dogs, anime, humor, gaming">
  <link rel="canonical" href="<?php echo $actual_link ?>" />
</head>
	<title><?php $title ?></title>

<body>
	Redirecting...
	<script>
	  window.location.replace('<?php echo $link ?>')
	</script>
</body>
</html>
