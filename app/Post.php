<?php namespace MemeVibe;

require_once __DIR__ . "/../vendor/autoload.php";

use MemeVibe\Activities as Activities;
use MemeVibe\Base as Base;
use MemeVibe\Files as Files;
use MemeVibe\Reactions as Reactions;

class Post extends Base{
	
	public $id;
	public $user_id;
	public $caption;
	public $source;
	public $shares;
	public $tags;
	public $reacts;
	public $isVid;
	public $nsfw;
	public $thumbnail;
	public $score;
	public $views;
	public $under_review;
	public $reviewer;
	public $featured;
	public $created;
	public static $collection = "Posts";
	
	const POST_ALLOW = 'allow';
	const POST_ALLOW_UPDATE = 'allow_update';
	const POST_REMOVE = 'remove';
	const POST_REMOVE_BAN = 'remove_ban';
	
	public function __construct($id=''){
		$this->id = $id;
	}
	
	public static function find($params=[], $user_id="", $get_nsfw = false, $viewer_id=""){
		
		/*
			Ranking algorithm
			Score = (P-1) / (T+2)^G
			
			P = points of an item (and -1 is to negate submitters vote)
			T = time since submission (in hours)
			G = Gravity, defaults to 1.8 in news.arc
		*/
		
		$default=[
			"params"=>[],
			'limit' => 10,
			'skip' => 0,
			'sort' => ['score' => -1, '_id'=>-1],
		];
		
		$options = self::set_params($default, $params);
		
		foreach($options['params'] as $k=>$v){
			if($k == "_id" && !is_array($v)){
				$options['params']['_id'] = new \MongoDB\BSON\ObjectId($v);
			}
		}
		
		$collection = self::get_collection(self::$collection);
		$aggregate = [];
		
		if(isset($options['params']) && !empty($options['params'])){
			$aggregate[]['$match'] = $options['params'];
		}
		
		if(!$get_nsfw){
			$aggregate[]['$match']['nsfw'] = null;
		}
		
		$aggregate[]['$project'] = [
			'user_id'=>1,
			'caption'=>1,
			'source'=>1,
			'score'=>1,
			'reacts'=>[
				'$map'=>[
					'input'=>'$reacts',
					'as'=> 'reacts',
					'in'=>[
						'id'=>'$$reacts.id',
						'path'=>'$$reacts.path',
						'name'=>'$$reacts.name',
						'count'=>['$size'=> ['$ifNull'=>['$$reacts.likes', [] ]] ],
						'liked'=>[ '$in'=>[$viewer_id, ['$ifNull'=>['$$reacts.likes', [] ]] ] ],
            'default'=>'$$reacts.default'
					]
				]
			],
			'tags'=>1,
			'comments'=>1,
			'isVid'=>1,
			'score'=>1,
			'featured'=>1,
			'views'=>1,
			'saves'=>[
				'$size'=>[
					'$ifNull'=>['$saved', [] ]]
			],
			'saved'=>[ '$in'=>[$viewer_id, ['$ifNull'=>['$saved', [] ]] ] ],
			'thumbnail'=>1
		];
		
		$aggregate[]['$sort'] = $options['sort'];
		$aggregate[]['$limit'] = (int)$options['limit']  + (int)$options['skip'] ;
		$aggregate[]['$skip'] = (int)$options['skip'];
		
		$cursor = $collection->aggregate($aggregate);
		if(empty($cursor)){
			return [];
		}
		
		$objs = [];
		foreach($cursor as $post){ 
			// Get reaction by id
			$ids = [];
			foreach($post['reacts'] as $r){
				$ids[] = $r['id'];
			}

			$objs[] = self::instantiate($post);
		}
		foreach($objs as $obj){
			$obj->increment_views();
		}
		return $objs;
	}
	
	public static function find_by_user($user_id, $get_count_only=false){
		$collection = self::get_collection(self::$collection);
		
		$aggregate[]['$match'] = ['user_id'=> $user_id];
		if($get_count_only){
			$aggregate[]['$project'] = ['_id'=> 0, 'count'=>['$sum'=>1]];
		}else{
			$aggregate[]['$project'] = [
				'user_id'=>1,
				'caption'=>1,
				'source'=>1,
				'score'=>[],
				'reacts'=>[
					'$map'=>[
						'input'=>'$reacts',
						'as'=> 'reacts',
						'in'=>[
							'id'=>'$$reacts.id',
							'count'=>['$size'=> ['$ifNull'=>['$$reacts.likes', [] ]] ],
							'liked'=>[ '$in'=>[$user_id, ['$ifNull'=>['$$reacts.likes', [] ]] ] ]
						]
					]
				],
				'tags'=>1,
				'comments'=>1,
				'isVid'=>1,
				'score'=>1,
				'views'=>1,
				'thumbnail'=>1
			];
		}
		
		$cursor = $collection->aggregate($aggregate);
		$posts = [];
		if($get_count_only){
			foreach($cursor as $r){
				return $r['count'];
			}
		}else{
			foreach($cursor as $r){
				$posts[] = self::instantiate($r);
			}
			foreach($posts as $post){
				$post->increment_views();
			}
			return $posts;
		}
		
	}
	
	public static function create($params=[]){
		$x = new self();
		if($x->validate_form($params)){
			
			// Remove duplicates in Reacts without losing its order.
			$params['reacts'] = $x->remove_duplicate_reacts($params['reacts']);
			$params['last_modified'] =  new \MongoDB\BSON\UTCDateTime((new \DateTime())->getTimestamp()*1000);
			
			$ext = strtolower(pathinfo($params['source'], PATHINFO_EXTENSION));
			if(in_array($ext,['mp4', 'mpeg', 'mov', 'm4v'])){ 
				$params["isVid"] = true;
				// Generate thumbnail
				$fs = new Files();
				
				if($thumb_path = $fs->get_video_thumbnail($fs->url_to_path($params['source']))){
					$params['thumbnail'] = $fs->path_to_url($thumb_path);
				}
			}
			$post = new Post();
			$post->created = time();
			$post->reacts= [];
			$params['score'] = $post->calculate_score();

			// Get full reacts
			$ids = [];
			foreach($params['reacts'] as $react){
				$ids[] = $react['id'];
			}
			
			$reacts = Reactions::find($ids);
			// Set original positions of reactions
			$params['reacts']=[];
			foreach($ids as $id){
				foreach($reacts as $react){
					if($react->id == $id && !isset($react->default)){
						$params['reacts'][] = $react;
						break;
					}
				}
			}
			
			if($inserted = self::get_collection(self::$collection)->insertOne($params)){
				//Add to activity
				$post_id = (string)$inserted->getInsertedId();
				
				try{
					$act_id = Activities::create([
						'user_id'=>$params['user_id'],
						'post_id'=>$post_id,
						'type'=>Activities::TYPE_POST
					]);
					if(!$act_id){
						$post = new Post($post_id);
						$post->remove();
						return false;	
					}
				}catch(\Exception $e){
					// delete this incomplete
					$post = new Post($post_id);
					$post->remove();
					return false;
				}
				
				return $post_id;
			}else{
				throw new \Exception("Failed creating post.");
			}
			
			
		}else{
			return false;
		}
	}
	
	public function update(){
		$collection = self::get_collection(self::$collection);
		
		$result = $collection->updateOne(
			['_id'=> new \MongoDB\BSON\ObjectId($this->id)],
			[ 
				'$set'=>[
					'user_id' => $this->user_id,
					'caption' => $this->caption,
					'tags' => $this->tags,
					'isVid' => $this->isVid,
					'reacts'=> $this->reacts
				]
			]
		);
		
		return $result->getMatchedCount() > 0 ? true : false ;
	}
	
	public function remove(){
		$collection = self::get_collection(self::$collection);
		$fs = new Files();
		
		$result = $collection->deleteOne(['_id'=>new \MongoDB\BSON\ObjectId($this->id)]);
		if($result->getDeletedCount() > 0){
			if(isset($this->source) && !empty($this->source)){
				$source_path = $fs->url_to_path($this->source);
				if(is_file($source_path)){
					unlink($source_path);
				}
			}
			if(isset($this->thumbnail) && !empty($this->thumbnail)){
				$thumb_path = $fs->url_to_path($this->thumbnail);
				if(is_file($source_path)){
					unlink($thumb_path);
				}
			}

			return $result->getDeletedCount() > 0 ? true : false ;
		}
	}
	
	public static function get_posts_count(Array $user_ids){
		$collection = self::get_collection(self::$collection);
		
		$cursor = $collection->aggregate([
			['$match'=>['user_id'=>['$in'=>$user_ids]]],
			['$project'=>[ '_id'=>0, 'user_id'=>1 ]]
		]);
		$post_counts=[];
		foreach($cursor as $post){
			$post_counts[$post['user_id']] = (isset($post_counts[$post['user_id']]) ? $post_counts[$post['user_id']]++ : 0) +1;
		}
		return $post_counts;
	}
	
	public static function get_under_review($offset = 0){
		$options=[
			"params"=>[
				'under_review'=>true
			],
			'limit' => 1 + $offset,
			'skip' => $offset,
			'sort' => ['_id'=>1],
		];
		
		$collection = self::get_collection(self::$collection);
		$aggregate = [];
		
		if(isset($options['params']) && !empty($options['params'])){
			$aggregate[]['$match'] = $options['params'];
		}
		
		$aggregate[]['$project'] = [
			'user_id'=>1,
			'caption'=>1,
			'source'=>1,
			'reacts'=>[
				'$map'=>[
					'input'=>'$reacts',
					'as'=> 'reacts',
					'in'=>[
						'id'=>'$$reacts.id',
						'count'=>['$size'=> ['$ifNull'=>['$$reacts.likes', [] ]] ],
					]
				]
			],
			'user'=>1,
			'tags'=>1,
			'isVid'=>1,
			'under_review'=>1,
			'thumbnail'=>1
		];
		
		$aggregate[]['$sort'] = $options['sort'];
		$aggregate[]['$limit'] = (int)$options['limit']  + (int)$options['skip'] ;
		$aggregate[]['$skip'] = (int)$options['skip'];
		
		$cursor = $collection->aggregate($aggregate);
		if(empty($cursor)){
			return [];
		}
		
		$objs = [];
		
		foreach($cursor as $post){ 
			if($post){
				$user = User::find($post->user_id);

				$post->user = $user->get_minimal();
				$post->user['posts'] = Post::find_by_user($user->id, true);
				$objs[] = self::instantiate($post);
			}
		}
		return $objs;
	}
	
	public static function mark_reviewed($id){
		$collection = self::get_collection(self::$collection);
		
		$result = $collection->updateOne(['under_review'=>true],[
			'$unset'=>[ 
				'under_review'=>1
			]
		]);
		
		return $result->getMatchedCount() > 0 ? true : false ;
	}
	
	private function validate_form($form){
		$whitelisted_fields = ['caption', 'user_id', 'source', 'reacts', 'thumbnail', 'tags', 'nsfw', 'under_review' ];

		foreach($form as $k=>$v){
			if($k == 'user_id'){
				if(!ctype_xdigit($v)){
					throw new \Exception("Invalid user ID");	
				}
			}
			
			if($k == 'tags'){
				if(count($v) < 2){
					throw new \Exception("Minimum tags should be atleast 2");	
				}
			}
			
			if($k == 'reacts'){
				if(count($v) < 1){
					throw new \Exception("Minimum reacts should be atleast 1");	
				}  
			}
			
			if(!in_array($k, $whitelisted_fields)){
				throw new \Exception("Can't create/update field $k");
			}
		}
		return true;
	}

	private function remove_duplicate_reacts($reacts){
		$ids=[];

		foreach($reacts as $k=>$react){
			if(!in_array($react['id'], $ids)){
				$ids[] = $react['id'];
			}else{
				unset($reacts[$k]);
			}
		}
		return $reacts;
	}

	private function calculate_score(){
		// Score
		$s = 0;
		foreach($this->reacts as $r){
			if($r->count){
				$s = $s + $r->count;
			}
		}
		if($s == 0){
			$s = -1;
		}

		// Sign
		if($s > 0){
			$sign = 1;
		}elseif($s < 0){
			$sign = -1;
		}else{
			$sign = 0;
		}

		// Order
		$order = log10(abs($s));
		$seconds = $this->created;

		// Score
		$score = round(  ($order * $sign) + ($seconds / 45000), 7);
		return $score;
	}

	public function update_score(){
		$collection = self::get_collection(self::$collection);
		$score = $this->calculate_score();
		
		$update = $collection->updateOne(['_id'=>new \MongoDB\BSON\ObjectId($this->id)],
		[
			'$set'=>['score'=>$score],
			'$inc'=>['views'=>1]
		]);
		
		if($update->getMatchedCount()){
			$this->score = $score;
			return true;	
		}else{
			return false;
		}
	}

	public function increment_views(){
		$collection = self::get_collection(self::$collection);

		$update = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($this->id)],
			[
				'$inc'=>[ 'views'=>1]
			],[
				'upsert'=>true
			]
		);

		if($update->getMatchedCount()){

			$this->views++;
			return true;
		}else{
			return false;
		}
	}

	public function react($user_id,$react_id){
		if(!ctype_xdigit($user_id)){
			throw new \Exception("Invalid user_id");
		}
		if(!ctype_xdigit($react_id)){
			throw new \Exception("Invalid react_id");
		}	
		$collection = self::get_collection(self::$collection);
		
		$remove_reacts = $collection->updateOne(
			['_id'=> new \MongoDB\BSON\ObjectId($this->id)],
			['$pull'=> ['reacts.$[el].likes'=> (string)$user_id]],
			[
				'arrayFilters'=>[
					['el.likes'=>$user_id],
				]
			]
		);
		
		$result = $collection->updateOne(
			[
				'_id'=> new \MongoDB\BSON\ObjectId($this->id),
				'reacts' => ['$elemMatch'=>[	'id'=> $react_id ]]
			],
			[ 
				'$addToSet'=>[ 
					'reacts.$.likes'=> (string)$user_id
				] 
			]
		);
    
    if(!$result->getMatchedCount()){
      $reaction = Reactions::find([$react_id])[0];
      $reaction->likes[] = $user_id;

      // Push into current object
			$this->reacts[] = $reaction;
      $result = $collection->updateOne(
        [
          '_id'=> new \MongoDB\BSON\ObjectId($this->id),
        ],
        [ 
          '$addToSet'=>[ 
            'reacts'=>$reaction
          ] 
        ]
      );
    }
		$id = $result->getMatchedCount();
		if($id){
			try{
				$act_id = Activities::create([
					'user_id'=>$user_id,
					'post_id'=>$this->id,
					'type'=>Activities::TYPE_REACT
				]);
				foreach($this->reacts as $r){
					if($r->id == $react_id){
						$r->count++;
					}
				}
				$this->update_score();
			}catch(\Exception $e){
				
			}
		}
		return $id;
	}
	
	public function remove_react($user_id, $react_id){
		if(!ctype_xdigit($user_id)){
			throw new \Exception("Invalid parameters user_id");
		}
		if(!ctype_xdigit($react_id)){
			throw new \Exception("Invalid parameters react_id");
		}
		
		$collection = self::get_collection(self::$collection);
		$result = $collection->updateOne(
			[
				'_id'=> new \MongoDB\BSON\ObjectId($this->id),
			],
			[ 
				'$pull'=>[ 
					'reacts.$[el].likes'=> (string)$user_id
				] 
			],
			['arrayFilters'=>[
					['el.likes' => (string)$user_id]
				]
			]
		);
		
		if($result->getMatchedCount()){
			foreach($this->reacts as $r){
				if($r->id == $react_id){
					$r->count--;
				}
			}
			$this->update_score();
			return Activities::remove('', Activities::TYPE_REACT, $this->id);
		}
	}
	
	public static function get_featured(){
		$collection = self::get_collection(self::$collection);
		
		$cursor = $collection->find(['featured'=>['$exists'=>true]],[
			'limit'=>15
		]);
	
		if(empty($cursor)) return [];
		
		$posts = [];
		foreach($cursor as $post){
			$posts[] = self::instantiate($post);
		}
		return $posts;
	}
	
	public static function mark_as_featured($id){
		$collection = self::get_collection(self::$collection);
		
		$update = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($id)],[
			'$set'=>[
				'featured'=> new \MongoDB\BSON\UTCDateTime()
			]
		]);
		if($update){
			return $update->getMatchedCount() > 0 ? true : false ;
		}
		return false;
	}
	
	public static function remove_from_featured($id){
		$collection = self::get_collection(self::$collection);
		
		$update = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($id)],[
			'$unset'=>[
				'featured'=> new \MongoDB\BSON\UTCDateTime()
			]
		]);
		if($update){
			return $update->getMatchedCount() > 0 ? true : false ;
		}
		return false;
	}
	
	public static function put_under_review($id){
		$collection = self::get_collection(self::$collection);
		
		$update = $collection->updateOne(
			['_id'=> new \MongoDB\BSON\ObjectId($id)],
			[
				'$set'=>[
					'under_review'=>1
				]
			]
		);
		
		return $update->getMatchedCount() ? $update->getMatchedCount() : false ;
	}
	
	public static function mark_as_reviewed($post, $user_id, $action = 'allow'){
		$collection = self::get_collection(self::$collection);
		
		if($action == self::POST_ALLOW){
			$parameters = [
				'$set'=>[ 'reviewer'=>['user_id'=> $user_id, 'timestamp'=> new \MongoDB\BSON\UTCDateTime()] ],
				'$unset'=>[ 'under_review'=>true]
			];
		}
		
		if($action == self::POST_REMOVE || $action == self::POST_REMOVE_BAN){
			if($action == self::POST_REMOVE_BAN){
				// Ban User
			}
			return $post->remove();
		}
		
		if($action == self::POST_ALLOW_UPDATE){
			$parameters = [
				'$set'=>[ 'reviewer'=>['user_id'=> $user_id, 'timestamp'=> new \MongoDB\BSON\UTCDateTime()],
					'caption'=> $post->caption,
					'tags'=>$post->tags,
					'nsfw'=>$post->nsfw
				],
				'$unset'=>[ 'under_review'=>true]
			];
		}
		
		$update = $collection->updateOne(
			['_id'=> new \MongoDB\BSON\ObjectId($post->id)],
			$parameters
		);
		
		return $update->getMatchedCount() ? true : false ; 
	}
	
	protected static function instantiate($result){ 
		$obj = new self;
		
		foreach($result as $k=>$v){
			if($k == "_id" || $k == "id"){
				$k = "id"; $v = (string) $v;
				$obj->created = (new \MongoDB\BSON\ObjectId($v))->getTimestamp();
			}
			$obj->$k = $v;
		}
		return $obj;
	}
}

//echo "<pre>";
//print_r(Post::find());

?>