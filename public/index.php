<?php 

$http_origin = $_SERVER['HTTP_ORIGIN'] ?? $_SERVER['HTTP_ORIGIN'] ?? $_SERVER['HTTP_REFERER'] ?? "";

if ($http_origin == "http://localhost:4200" || $http_origin == "https://dopachan.com" || $http_origin == "https://api.dopachan.com"){  
	header("Access-Control-Allow-Origin: $http_origin");
}

header("Access-Control-Allow-Methods: OPTIONS, POST, PUT, GET, DELETE");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: authorization, X-Requested-With,content-type");
header("Content-Type: application/json");

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ .  '../../vendor/autoload.php';

$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);

$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");

    return $response;
});

// Posts Routes
require_once __DIR__ ."/../src/routes/posts.php";
// User Routes
require_once __DIR__ ."/../src/routes/users.php";
// Comment Routes
require_once __DIR__ ."/../src/routes/comments.php";
// Reactions Routes
require_once __DIR__ ."/../src/routes/reactions.php";

$app->run();