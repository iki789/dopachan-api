<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \MemeVibe\Comment as Comment;
use \MemeVibe\Post as Post;
use \MemeVibe\Report as Report;
use \MemeVibe\Files as Files;
use \MemeVibe\Config as Config;
use \MemeVibe\User as User;
use \MemeVibe\JWT as Jwt;

require_once __DIR__ . '/../../vendor/autoload.php';

$app->get('/api/posts[/section/{section}]', function (Request $request, Response $response, Array $args) {
	
	$body = $request->getQueryParams();

	$options = [];
	$options['sort']['score'] = isset($body['sort']) && !empty($body['sort']) && is_numeric($body['sort']) ? (int)$body['sort'] : (int)1 ;
	$options['limit'] = isset($body['limit']) && !empty($body['limit']) && is_numeric($body['limit']) ? (int)$body['limit'] : (int)5 ;
	$options['skip'] = isset($body['skip']) && !empty($body['skip']) && is_numeric($body['skip']) ? (int)$body['skip'] : (int)0 ;
	
	$options['params']['under_review'] = null ;
	$get_nsfw = false;
	
	if(isset($args['section']) && !empty($args['section'])){
		$options['params'] = [
			'$text'=>['$search'=>$args['section'] ]
		];
	}	
	
	$jwt = new Jwt();
	if(!empty($jwt->token)){
		$user = $jwt->get_user();
		
		if((isset($user['age_group']) && $user['age_group']) > 17 || $user['group'] > 60 ){
			$get_nsfw = true;
		}
	}
	
	$user_id = isset($user['id']) && !empty($user['id']) ? $user['id'] : "" ;
	
	$posts = Post::find($options, $user_id, $get_nsfw, $user_id);
	
	foreach($posts as $post){
		$post->comments = Comment::get_preview($post->id, $user_id);
	}
	return $response->withJson($posts);
});

$app->get('/api/post/{id}', function(Request $request, Response $response, Array $args){
	if(!ctype_xdigit($args['id'])) return $response->withJson([]);

	$options = [];
	$options['limit'] = 1;
	$options['params']['under_review'] = null ;
	$options['params']['_id'] = $args['id'] ;
	$get_nsfw = false;
	
	$jwt = new Jwt();
	if(!empty($jwt->token)){
		$user = $jwt->get_user();
		
		if((isset($user['age_group']) && $user['age_group']) > 17 || $user['group'] > 60 ){
			$get_nsfw = true;
		}
	}
	
	$user_id = isset($user['id']) && !empty($user['id']) ? $user['id'] : "" ;
	
	$posts = Post::find($options, $user_id, $get_nsfw, $user_id);
	
	foreach($posts as $post){
		$comment_options['params']['post_id']=$post->id;
		$comment_options['limit']=0;
		$post->comments = Comment::find($comment_options, $user_id);
		if(is_array($post->comments)){
			foreach($post->comments as $i=>$comment){
				$comment->likes = count($post->comments[$i]->likes);
				$comment->dislikes = count($post->comments[$i]->dislikes);
			}
		}
	}
	return $response->withJson($posts);
});

$app->post('/api/post', function (Request $request, Response $response) {
	$jwt = new Jwt();
	
	if(empty($jwt->token)) return $response->withStatus(401);
	
	$user = $jwt->get_user();
	$data = $request->getParsedBody();
	
	$fs = new Files();
	$filename = basename($data['file']['path']);
	
	if(!$source = $fs->move_from_tmp($filename)){
		return $response->withJson(['success'=>0, 'error'=>"File was not moved"]);
	}
	
	$post = [
		'user_id'=> $user['id'],
		'caption'=> $data['caption'],
		'source' => $fs->path_to_url($source),
		'reacts'=> $data['reacts'],
		'tags' => $data['tags'],
	];
	
	if($data['nsfw']){
		$post['nsfw'] = true;
	}
	
	if($user['group'] < 40){
		$post['under_review'] = true ;
	}
	
	$post_class = new Post();
	$result = $post_class->create($post);
	if($result){
		$response->getBody()->write(json_encode(["success"=>1, "id"=> $result]));
	}else{
		$response->getBody()->write(json_encode(["success"=>0, "error"=>$result]));
	}
});

$app->delete('/api/post/{id}', function(Request $request, Response $response, Array $args){
	$jwt = new Jwt();
	if(empty($jwt->token)) return $response->withStatus(401);
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_SENIOR_ADMIN) return $response->withStatus(401);
	$id = $args['id'];
	$body = $request->getQueryParams();
	
	$post = new Post($id);
	if($post->remove()){
		if(isset($body['poster_id'])){
			$poster = new User($body['poster_id']);
			if(isset($body['ban']) && $body['ban']==true){
				if(!$poster->ban($user['id'], $user['group'], $body['reason'])){
					return $reponse->withJson(['error'=>'User not banned but post has been removed']);
				}
			}else{
				if(!$poster->strike($user['id'], $user['group'], $body['reason'])){
					return $reponse->withJson(['error'=>'User didn\'t get striked but post has been removed']);
				}
			}
		}
		return $response->withJson(['success'=>1]);
	}else{
		return $reponse->withJson(['error'=>'Failed removing post']);
	}
});

$app->post('/api/posts/{post_id}/react', function(Request $request, Response $response, Array $args){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$body = $request->getParsedBody();
	$user = $jwt->get_user();
	$post_id = $args['post_id'];
	$react_id = $body['id'];
	
	// Add reaction
	if(!isset($body['to_remove']) || $body['to_remove'] != 1){
		$post = Post::find([
			'params'=>['_id'=>$post_id],
			'limit'=>1
		]);
		if($post[0]->react($user['id'], $react_id)){
			return $response->withJson(['success'=>1]);
		}else{
			return $response->withJson(['error'=>'Something went wrong!']);
		}
	}else if(isset($body['to_remove']) && $body['to_remove'] == 1){
		// Remove reaction
		$post = Post::find([
			'params'=>['_id'=>$post_id],
			'limit'=>1
		]);
		if($post[0]->remove_react($user['id'], $react_id)){
			return $response->withJson(['success'=>1, 'action'=>'removed']);
		}else{
			return $response->withJson(['error'=>'Something went wrong!']);
		}
	}
	
});

$app->post('/api/post/upload', function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)) return $response->withStatus(401);
	
	$files = $request->getUploadedFiles();

	// Create a $_FILES array to compensate with API
	$uploaded_file['tmp_name'] = $files['file']->file;
	$uploaded_file['name'] = $files['file']->getClientFilename();
	$uploaded_file['size'] = $files['file']->getSize();
	$uploaded_file['error'] = $files['file']->getError();
	$uploaded_file['type'] = $files['file']->getClientMediaType();

	if ($files['file']->getError() === UPLOAD_ERR_OK) {
		$fs = new Files();
		$upload_path = $fs->upload_to_tmp($uploaded_file);
		if($upload_path){
			$response->getBody()->write(json_encode(["success"=>1, "path"=>($fs->path_to_url($upload_path))]));
		}else{
			$response->getBody()->write(json_encode(["success"=>0, "error"=>"Couldn't move file!"]));
		}	
	}else{
		$response->getBody()->write(json_encode(["success"=>0, "error"=> $uploaded_file->getError()]));
	}
});

$app->get('/api/featured/posts', function(Request $request, Response $response){
	$posts = Post::get_featured();
	
	if($posts){
		return $response->withJson($posts);
	}else{
		return $response->withJson([]);
	}
});

$app->post('/api/featured/posts/{id}', function(Request $request, Response $response, Array $args){
	$jwt = new Jwt();
	if(empty($jwt->token)) return $response->withStatus(401);
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_SENIOR_ADMIN) return $response->withStatus(401);
	
	if(Post::mark_as_featured($args['id'])){
		return $response->withJson(['success'=>1]);
	}else{
		return $response->withJson(['error'=>1]);
	}
});

$app->delete('/api/featured/posts/{id}', function(Request $request, Response $response, Array $args){
	$jwt = new Jwt();
	if(empty($jwt->token)) return $response->withStatus(401);
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_SENIOR_ADMIN) return $response->withStatus(401);
	
	if(Post::remove_from_featured($args['id'])){
		return $response->withJson(['success'=>1]);
	}else{
		return $response->withJson(['error'=>1]);
	}
});

$app->post('/api/post/report/{id}', function(Request $request, Response $response, Array $args){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$body = $request->getParsedBody();
	$user = $jwt->get_user();
	$poster_id = $body['poster_id'];
	$reason = isset($body['reason']) && !empty($body['reason']) ? $body['reason'] : "" ;
	switch($body['reason']){
		case 1:
			$reason = Report::SPAM;
			break;
		case 2:
			$reason = Report::PORNOGRAPHY;
			break;
		case 3:
			$reason = Report::HATRED_BULLYING;
			break;
		case 4:
			$reason = Report::SELF_HARM;
			break;
		case 5:
			$reason = Report::VIOLENT;
			break;
		case 6:
			$reason = Report::ILLEGAL_ACTIVITIES;
			break;
		case 7:
			$reason = Report::DECEPTIVE;
			break;
		case 8:
			$reason = Report::COPYRIGHT;
			break;
		case 9:
			$reason = Report::I_DONT_LIKE_IT;
			break;
		default:
			$reason = "";
			break;
	}
	
	if(empty($reason)){
		return $response->withJson(['error'=>"Not a valid reason"]);
	}
	
	$report = new Report();
	$report->issuer_id = $user['id'];
	$report->issue = $reason;
	$report->poster_id = $poster_id;
	$report->content_id = $args['id'];
	$report->type = Report::TYPE_POST;
	
	if($id = $report->create()){
		return $response->withJson(['success'=>1, 'id'=>$id]);
	}else{
		return $response->withJson(['success'=>0]);
	}
	
});

$app->post('/api/post/save/{id}', function(Request $request, Response $response, Array $args){
	$post_id = $args['id'];
	
	$jwt = new Jwt();
	if(empty($jwt->token)) return $response->withStatus(401);
	$user = $jwt->get_user();
	
	$user = new User($user['id']);
	if($user->add_to_collection($post_id)){
		return $response->withJson(['success'=>1]);
	}else{
		return $response->withJson(['error'=>1]);
	}
});

$app->post('/api/post/saved/remove/{id}', function(Request $request, Response $response, Array $args){
	$post_id = $args['id'];
	
	$jwt = new Jwt();
	if(empty($jwt->token)) return $response->withStatus(401);
	$user = $jwt->get_user();
	
	$user = new User($user['id']);
	if($result = $user->remove_from_collection($post_id)){
		return $response->withJson(['success'=>1]);
	}else{
		return $response->withJson(['error'=>1]);
	}
});

$app->get('/api/reports/post', function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_ADMIN){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	
	$q = $request->getQueryParams();
	$skip = isset($q['skip']) ? (int)$q['skip'] : 0 ;
	$report = Report::get_posts(['skip'=>$skip]);
	if(empty($report)) return $response->withJson([]);
	// Send friendly date
	switch($report->issue){
		case 1:
			$report->issue = Report::SPAM;
			break;
		case 2:
			$report->issue = Report::PORNOGRAPHY;
			break;
		case 3:
			$report->issue = Report::HATRED_BULLYING;
			break;
		case 4:
			$report->issue = Report::SELF_HARM;
			break;
		case 5:
			$report->issue = Report::VIOLENT;
			break;
		case 6:
			$report->issue = Report::ILLEGAL_ACTIVITIES;
			break;
		case 7:
			$report->issue = Report::DECEPTIVE;
			break;
		case 8:
			$report->issue = Report::COPYRIGHT;
			break;
		case 9:
			$report->issue = Report::I_DONT_LIKE_IT;
			break;
		default:
			$report->issue = "";
			break;
	}
	$report->created = Config::contextual_time($report->created);
	$report->post->created = Config::contextual_time($report->post->created);
	
	return $response->withJson($report);
});
		
$app->post('/api/reports/post/allow', function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_ADMIN){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$body = $request->getParsedBody();
	$report_id = $body['report_id'];
	$content_id = $body['content_id'];
	
	$report = Report::find([
		'filters'=>[
			'_id'=> $report_id,
			'content_id'=>$content_id
		]
	]);
	
	return $report[0]->allow();
	
});

$app->post('/api/reports/post/remove', function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_ADMIN){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	
	$body = $request->getParsedBody();
	$report_id = $body['report_id'];
	$content_id = $body['content_id'];
	$type = $body['type'] == 'post' ? Report::TYPE_POST : Report::TYPE_COMMENT;

	$report = new Report();
	$report = (Report::find(['filters'=>['_id'=>$report_id]]))[0];
	
	$remove = $report->remove();
	
	if($remove && isset($query_params['ban']) && !empty($query_params['ban'])){
		if(isset($query_params['reason']) && !empty($query_params['reason']) ){
			$user = new User($query_params['ban']);
			$banned = $user->ban($user['id'], $user['group'], $query_params['reason']);
			if($banned){
				return $response->withJson(['success'=>1]);
			}else{
				return $response->withJson(['error'=>'Post removed but user not banned']);
			} 
		}else{
			return $response->withJson(['error'=>'User not banned. Reason required for banned.']);
		}
	}
	
	return $remove ? $response->withJson(['success'=>1]) : $response->withJson(['error'=>1]); 
	
});

$app->get('/api/reviews/post', function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_ADMIN){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	
	$q = $request->getQueryParams();
	$skip = isset($q['skip']) ? (int)$q['skip'] : 0 ;
	$report = Post::get_under_review($skip);
	if(empty($report)) return $response->withJson([]);
	
	
	return $response->withJson($report);
});

$app->post('/api/reviews/post/allow', function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)) return $response->withStatus(401);
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_ADMIN) return $response->withStatus(401);
	$query_params = $request->getQueryParams();
	$action = Post::POST_ALLOW;
	$body = $request->getParsedBody();
	
	$post = new Post($body['id']);
	if(isset($query_params['save']) && $query_params['save']){
		$action = Post::POST_ALLOW_UPDATE;
		$post->caption = $body['caption'];
		$post->nsfw = $body['nsfw'];
		$post->tags = $body['tags'];
	}
	
	return Post::mark_as_reviewed($post, $user['id'], $action);
	
});

