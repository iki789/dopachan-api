<?php namespace MemeVibe;

require_once __DIR__ . "../../vendor/autoload.php";

class Base{
		
	public static function get_collection($collection){
		return (new \MongoDB\Client(Config::DB_HOST))->MemeVibe->$collection;
	}
	
	protected static function set_params($default, $user_defined){ 
		foreach($default as $k => $v){
			if(isset($user_defined[$k])){
				$default[$k] = $user_defined[$k];
			}
		}
		return $default;
	}
	
	protected static function instantiate($result){ 
		$obj = new self;
		foreach($result as $k=>$v){
			if($k == "_id" || $k == "id"){
				$k = "id"; $v = (string) $v;
			}
			
			$obj->$k = $v;
		}
		return $obj;
	}
	
}

?>