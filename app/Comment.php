<?php namespace MemeVibe;

require_once __DIR__ .  "/../vendor/autoload.php";

use MemeVibe\Activities as Activities;

class Comment extends Base{
	
	public $id;
	public $post_id;
	public $user;
	public $replay_for;
	public $comment;
	public $likes;
	public $dislikes;
	private static $collection = "Comments";
	
	public function __contruct($id, $post_id, $user, $reply_for, $comment, $likes){
		$this->id = $id;
		$this->post_id = $id;
		$this->user = $user;
		$this->replay_for = $reply_for;
		$this->comment = $comment;
		$this->likes = $likes;
	}
	
	public static function create($user, $post_id, $comment){
		$collection = self::get_collection(self::$collection);
		$comment = trim($comment);
		$inserted = $collection->insertOne([
			"user" => new \MongoDB\Model\BSONDocument([
				'id' => $user['id'],
				'display_name' =>$user['display_name'],
				'avatar'=>$user['avatar']
			]),
			"post_id" =>$post_id,
			"comment" => $comment
		]);
		$id = $inserted->getInsertedId();
		
		if($inserted->getInsertedId()){
			try{
				$act_id = Activities::create([
					'user_id'=>(string)$user['id'],
					'post_id'=>(string)$post_id,
					'comment_id'=>$id,
					'type'=>Activities::TYPE_COMMENT
				]);
				
			}catch(\Exception $e){
				// delete this incomplete
				echo $e;
				self::remove($id, $user['id']);
				return false;
			}
		}
		return (string)$id;
	}
	
	public static function find($options=[], $user_id=""){
		$default = [
			"params"=>['under_review'=>null],
			"sort"=>[
				'likes'=>1
			],
			"limit"=>0
		];
		
		$ops = self::set_parameters($default, $options);
		if(!empty($user_id)){
			$op['user_id'] = $user_id;
		}
		foreach($options['params'] as $k=>$v){
			if($k=="_id" || $k=="id"){
				$ops['params'][$k] = new \MongoDB\BSON\ObjectId($v);
			}
		}
		
		$collection = self::get_collection(self::$collection);
		$results = $collection->find(
			$ops['params'],
			[
				$ops['sort'],
				'limit'=>$ops["limit"],
			]
		);
		$obj = [];
		foreach($results as $result){
			$obj[] = self::instantiate($result);
		}
		
		return $obj ;
	}
	
	public static function remove($id, $user_id){
		$collection = self::get_collection(self::$collection);
		
		$deleted = $collection->deleteOne(['_id'=> new \MongoDB\BSON\ObjectId($id), 'user.id' => $user_id]);

		if($deleted && $deleted->getDeletedCount() > 0){
			$del = Activities::remove('', Activities::TYPE_COMMENT, '',$id);
			return true;
		}else{
			return false;
		}
	}
	
	public static function put_under_review($id){
		$collection = self::get_collection(self::$collection);
		
		$result = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($id)],
			[
				'$set'=>['under_review'=>1]
			]
		);
		
		return $result->getMatchedCount() > 0 ? true : false ;
	}
	
	public static function remove_from_under_review($id){
		$collection = self::get_collection(self::$collection);
		
		$result = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($id)],
			[
				'$unset'=>['under_review'=>true]
			]
		);
		
		return $result->getMatchedCount() > 0 ? true : false ;
	}
	
	public static function update_user($user_id, $avatar_path, $display_name){
		$collection = self::get_collection(self::$collection);
		
		$result = $collection->updateMany(['user.id'=>$user_id],
			['$set'=>['user.avatar'=>(string)$avatar_path, 'user.display_name'=>(string)$display_name]]
	 	);
		
		if($result){
			return $result->getMatchedCount(); 
		}
		return false;
	}
	
	public static function get_preview($post_id, $user_id=""){
		
		$user_id = isset($user_id) ? $user_id : "no_id";

		$collection = self::get_collection(self::$collection);
		$result = $collection->aggregate([
			['$match'=> [
					'post_id'=> $post_id,
					'under_review'=> null
				]
			],
			['$project'=>[
					'_id'=>1,
					'post_id'=>1,
					'user'=>1,
					'likes'=>[ '$size'=>['$ifNull'=> [ '$likes', [] ]] ],
					'dislikes'=>[ '$size'=>['$ifNull'=> [ '$dislikes', [] ]] ],
					'liked'=>['$in'=>[$user_id, ['$ifNull'=>['$likes', []] ]] ],
					'disliked'=>['$in'=>[$user_id, ['$ifNull'=>['$dislikes', []] ]] ],
					'comment'=>'$comment'
				]
			],
			['$sort'=>['likes'=>-1]],
			['$limit'=>2]
		]);

		$to_return = [];
		foreach($result as $r){
			$to_return[] = self::instantiate($r);
		}
		return $to_return;
	}
	
	public static function like($id, $user_id){
		if(!ctype_xdigit($user_id)){ throw new \Exception("Invalid user ID"); }
		if(!ctype_xdigit($id)){ throw new \Exception("Invalid comment ID"); }
		
		$collection = self::get_collection(self::$collection);
		$result = $collection->updateOne(
			['_id' => new \MongoDb\BSON\ObjectId($id)],
			['$addToSet'=>['likes' =>$user_id], '$pull'=>['dislikes'=>$user_id]]
		);
		
		return $result->getMatchedCount() ? true : false ;
		
	}
	
	public static function dislike($id, $user_id){
		if(!ctype_xdigit($user_id)){ throw new \Exception("Invalid user ID"); }
		if(!ctype_xdigit($id)){ throw new \Exception("Invalid comment ID"); }
		
		$collection = self::get_collection(self::$collection);
		$result = $collection->updateOne(
			['_id' => new \MongoDb\BSON\ObjectId($id)],
			['$addToSet'=>['dislikes' =>$user_id], '$pull'=>['likes'=>$user_id]]
		);
		
		return $result->getMatchedCount() ? true : false ;
	}
	
	public static function remove_action($id, $user_id){
		if(!ctype_xdigit($user_id)){ throw new \Exception("Invalid user ID"); }
		if(!ctype_xdigit($id)){ throw new \Exception("Invalid comment ID"); }
		
		$collection = self::get_collection(self::$collection);
		$result = $collection->updateOne(
			['_id' => new \MongoDb\BSON\ObjectId($id)],
			['$pull'=>['likes'=>$user_id, 'dislikes' =>$user_id]],
			['multi'=>true]
		); 
		return $result->getMatchedCount() > 0 ? true : false ;
	}
	
	private static function set_parameters($default, $user_defined){
		foreach($default as $k => $v){
			if(isset($user_defined[$k])){
				$default[$k] = $user_defined[$k];
			}
		}
		return $default;
	}
	
	protected static function instantiate($result){ 
		$obj = new self;
		foreach($result as $k=>$v){
			if($k == "_id" || $k == "id"){
				$k = "id"; $v = (string) $v;
			}
			
			$obj->$k = $v;
		}
		return $obj;
	}
	
}

//echo "<pre>";
//print_r(Comment::find(['params'=>[
// 'user.id' => "5af0318d6925a32ef0000cbc"
//]]));

?>