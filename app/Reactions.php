<?php namespace MemeVibe;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Config.php';

use MemeVibe\Base as Base;
use MemeVibe\Files as Files;

class Reactions extends Base{
	public $id;
	public $name;
	public $path;
	public $position = 0;
	public $default;
	public static $collection = 'Reactions';
	private static $max_size = 1024*24;
	private static $extensions = ['jpeg', 'jpg', 'png'];
	
	public function __construct($id=""){
		$this->id = $id;
	}
	
	public function create($file_path){
		$collection  = self::get_collection(self::$collection);
		if($this->validate()){
			if($this->upload($file_path)){
				$last_position = self::get_last_position();
				$position = !empty($last_position) ? $last_position : 0 ;
				$insert = $collection->insertOne([
					'name'=>$this->name,
					'path'=>(new Files)->path_to_url($this->path),
					'position'=>(int)$position+1
				]);
				return $insert->getInsertedId() ? $insert->getInsertedId() : false ;
			}else{
				return false;
			}
		}
	}
	
	public static function find(Array $ids=[], int $limit=0, int $skip=0){
		$collection = self::get_collection(self::$collection);
		$filters = [];
		if(!empty($ids)){
			foreach($ids as $k=>$id){
				$ids[$k] = new \MongoDB\BSON\ObjectId($id);
			}
			$filters = ['_id'=>['$in'=>$ids]];
		}
		
		$result = $collection->find($filters,[
			'sort'=>['position'=>1],
			'skip'=>(int)$skip,
			'limit'=>(int)$limit,
		]);
		
		$reactions = [];
		foreach($result as $reaction){
			$reactions[] = self::instantiate($reaction);
		}
		return $reactions;
	}
	
	public function remove(): void{
		$collection = self::get_collection(self::$collection);
		$collection->deleteOne(['_id'=> new \MongoDb\BSON\ObjectId($this->id)]);

	}
	
	public static function get_default(){
		$collection = self::get_collection(self::$collection);
		
		$results = $collection->find(['default'=>true],['limit=>5']);
		$reactions=[];
		foreach($results as $reaction){
			$reactions[] = self::instantiate($reaction);
		}
		return $reactions;
	}
	
	public function mark_default(): bool{
		$collection = self::get_collection(self::$collection);

		$collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($this->id)],[
			'$set'=>[
				'default'=>true,
			]
		]);
		return true;
	}
	
	public function remove_default(): bool{
		$collection = self::get_collection(self::$collection);

		$collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($this->id)],[
			'$unset'=>[
				'default'=>true,
			]
		]);
		return true;
	}
	
	private static function get_last_position(){
		$collection = self::get_collection(self::$collection);
		$pos = $collection->findOne([],[
			'projection'=>['_id'=>0, 'position'=>1],
			'sort'=>['position'=>-1],
			'limit'=>1
		]);
		return isset($pos->position) ? $pos->position : false;
	}
	
	public function update_position(){
		$collection = self::get_collection(self::$collection);
		$update = $collection->updateOne(['_id'=>new \MongoDB\BSON\ObjectId($this->id)],[
			'$set'=>[
				'position'=> (int)$this->position
			]
		]);
		
		return $update->getMatchedCount() > 0 ? true : false ;
	
	}
	
	private function validate(): bool{
		// if position is an int
		if(!is_numeric($this->position)){
			throw new \Exception("'Position' should be an int, given '$this->position'");
		}
		// if name is not empty and more than 2 characters long
		if(empty(trim($this->name)) || strlen(trim($this->name)) < 3){
			throw new \Exception("Reaction name should be atleast 3 characters long, give '$this->name'");
		}
		// if file is valid and exists
//		if(is_file($path)){
//			$file = pathinfo($path);
//			$ext = strtolower($file['extension']);
//			if($ext != 'png' || $ext != 'jpg' || $ext != 'jpeg'){
//				throw new \Exception("File is not a valid, should be 'png', 'jpg' or 'jpeg', given '$ext'");
//			}
//		}else{
//			throw new \Exception("File not found, give '$this->path'");
//		}
		return true ;
	}
	
	private function upload($file_array){
		if(!is_file($file_array['tmp_name'])){
			throw new \Exception("File doesn't exitst.");
		}
		$file_info = pathinfo($file_array['tmp_name']);
		$file_path = $file_array['tmp_name'];
		$file_name = $file_array['name'];
		$ext = pathinfo($file_name)['extension'];
		$path = Config::REACTIONS_PATH;
		// Validate move directory
		if(!is_dir($path)){
			mkdir($path, 0755);	
		}
		if(!is_writable($path)){
			chmod($path, 0755);
		}
		if(!in_array(strtolower($ext), self::$extensions)){
			throw new \Exception("Invalid file. File supported are " . implode(', ', self::$extensions) . '.');
		}
		if(filesize($file_path) > self::$max_size){
			throw new \Exception('File size exceeds ' . self::$max_size/1024 . 'kbs');
		}
		//search for duplicates
		$reaction_files = scandir($path);
		$rand_name = (new Files())->generate_file_name();
		while(in_array($rand_name, $reaction_files)){	
			$rand_name = (new Files())->generate_file_name();
		}
		$final_path = $path . Config::DS . $rand_name . '.' . $ext;
		if(copy($file_path, $final_path)){
			$this->path = $final_path;
		}else{
			throw new \Exception("Failed moving file.". $path . " - " . $final_path);
		}
		return $final_path;
	}
	
}

?>