<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/../../vendor/autoload.php';

use \MemeVibe\Comment;
use \MemeVibe\Report;
use \MemeVibe\Config;
use \MemeVibe\Jwt;

$app->post("/api/comments/", function(Request $request, Response $response){
	$jwt = new Jwt();
		
	if(!isset($jwt->token)){ 
		$response->getBody()->write("", 401);
	}
	
	$data = $request->getParsedBody();
	$user = $jwt->get_user();

	$user = [
		"id"=>$user['id'],
		"display_name"=>$user['display_name'],
		"avatar"=>$user['avatar']
	];
	if($inserted_id = Comment::create($user, $data['post_id'], $data['comment'])){
		return $response->withJson(["success" => 1, "id" => (string)$inserted_id]);
	}else{
		return $response->withJson(["success" => 0]);
	}
	
});

$app->delete("/api/comments/{id}", function(Request $request, Response $response, Array $args){
	$id = $args['id'];
	$jwt = new Jwt();
	if(!isset($jwt->token)){ 
		return $response->write("")->withStatus(401);
	}
	$user = $jwt->get_user();
	
	$deleted = Comment::remove($id, $user['id']);
	if($deleted){
		return $response->withJson(['success'=>1]);	
	}else{
		return $response->withJson(['success'=>0]);
	}

});

$app->post("/api/comments/like", function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(["error" => 'Unauthorized'])->withStatus(401);	
	}
	$body = $request->getParsedBody();
	$user = $jwt->get_user();

	try{
		$update = Comment::like($body['id'], $user['id']);
		return $response->withJson(["success" => 1]);
	}catch(Exception $e){
		return $response->withJson(["error" => $e->getMessage()]);	
	}
	
});

$app->post("/api/comments/dislike", function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(["error" => 'Unauthorized'])->withStatus(401);	
	}
	$body = $request->getParsedBody();
	$user = $jwt->get_user();

	try{
		$update = Comment::dislike($body['id'], $user['id']);
		return $response->withJson(["success" => 1]);
	}catch(Exception $e){
		return $response->withJson(["error" => $e->getMessage()]);	
	}
	
});

$app->post("/api/comments/removelike", function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(["error" => 'Unauthorized'])->withStatus(401);	
	}
	$body = $request->getParsedBody();
	$user = $jwt->get_user();

	try{
		if($update = Comment::remove_action($body['id'], $user['id'])){
			return $response->withJson(["success" => 1]);	
		}else{
			return $response->withJson(['error'=>$update]);
		}
	}catch(Exception $e){
		return $response->withJson(["error" => $e->getMessage()]);	
	}
	
});

$app->post("/api/comment/report/{id}", function(Request $request, Response $response, Array $args){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$body = $request->getParsedBody();
	$user = $jwt->get_user();
	$reason = isset($body['reason']) && !empty($body['reason']) ? $body['reason'] : "" ;
	$content_id = isset($args['id']) && !empty($args['id']) ? $args['id'] : "" ;
	$poster_id = isset($body['poster_id']) && !empty($body['poster_id']) ? $body['poster_id'] : "" ;
	
	switch($body['reason']){
		case 1:
			$reason = Report::SPAM;
			break;
		case 2:
			$reason = Report::PORNOGRAPHY;
			break;
		case 3:
			$reason = Report::HATRED_BULLYING;
			break;
		case 4:
			$reason = Report::SELF_HARM;
			break;
		case 5:
			$reason = Report::VIOLENT;
			break;
		case 6:
			$reason = Report::ILLEGAL_ACTIVITIES;
			break;
		case 7:
			$reason = Report::DECEPTIVE;
			break;
		case 8:
			$reason = Report::COPYRIGHT;
			break;
		case 9:
			$reason = Report::I_DONT_LIKE_IT;
			break;
		default:
			$reason = "";
	}
	
	if(empty($reason)){
		return $response->withJson(['error'=>"Not a valid reason"]);
	}
	
	$report = new Report();
	$report->issuer_id = $user['id'];
	$report->issue = $reason;
	$report->poster_id = $poster_id;
	$report->content_id = $content_id;
	$report->type = Report::TYPE_COMMENT;
	
	if($report->create()){
		return $response->withJson(['success'=>1]);
	}else{
		return $response->withJson(['success'=>0]);
	}
	
});

?>