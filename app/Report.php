<?php 
declare(strict_types = 1);

namespace MemeVibe;

require_once __DIR__ ."/../vendor/autoload.php";

use MemeVibe\Base as Base;
use MemeVibe\Comment as Comment;
use MemeVibe\Post as Post;
				
class Report extends Base{
	public $id;
	public $issue;
	public $issuers;
	public $issuer_id;
	public $poster_id;
	public $content_id;
	public $post;
	public $user;
	public $type;
	public $created;
	private $count;
	private $resolution;
	private static $collection = "Reports";
	
	// Reasons
	const SPAM = 1;
	const PORNOGRAPHY = 2;
	const HATRED_BULLYING = 3;
	const SELF_HARM = 4;
	const VIOLENT = 5;
	const ILLEGAL_ACTIVITIES = 6;
	const DECEPTIVE = 7;
	const COPYRIGHT = 8;
	const I_DONT_LIKE_IT = 9;
	
	// Types
	const TYPE_COMMENT = 10;
	const TYPE_POST = 11;
	
	// Resolutions
	const RESOLUTION_DELETED = 12;
	const RESOLUTION_ALLOWED = 13;
	
	public function __construct(){
		if(empty($this->count)){ $this->count = 1; }
	}
	
	public function create(){
		$collection = self::get_collection(self::$collection);
		
		if($this->validate()){
			// Check if this is reported already
			$reported_already = self::find(['filters'=>['content_id'=> $this->content_id, 'type'=>$this->type]]);
			if(!empty($reported_already)){
				// Add issuer to list
				$this->add_issuer($reported_already[0]->id);
				$this->count++;
				if($reported_already[0]->count > 4){
					// Hide content
					if($this->type === self::TYPE_COMMENT){
						Comment::put_under_review($this->content_id);
					}elseif($this->type === self::TYPE_POST){
						Post::put_under_review($this->content_id);
					}
				}
				
				return (string)$reported_already[0]->id;
			}

			$insert = $collection->insertOne([
				'issuers'=> [$this->issuer_id],
				'poster_id'=>$this->poster_id,
				'issue'=>$this->issue,
				'content_id'=> $this->content_id,
				'type'=> $this->type
			]);
			
			return $insert->getInsertedId() ? $insert->getInsertedId() : false ;
		}
	}
	
	public static function find($params=[]){
		$collection = self::get_collection(self::$collection);
		$default=[
			"filters"=>[ 'resolution' => ['$exists'=> false] ],
			'limit' => 10,
			'skip' => 0,
			'sort' => ['_id' => 1],
		];
		
		$options = self::set_params($default, $params);
		
		// Replace _ids with object ids
		foreach($options['filters'] as $k=>$v){
			if($k == '_id'){
				$options['filters'][$k] = new \MongoDB\BSON\ObjectId($v);
			}
		}
			
		$aggregate[]['$match'] = $options['filters'];
		$aggregate[]['$project'] = [
			'issue'=>1,
			'issuers'=>1,
			'poster_id'=>1,
			'content_id'=>1,
			'type'=>1,
			'count'=>['$size'=>['$ifNull'=>['$issuers', [] ]] ],
			'resolved'=>1
		];
		$aggregate[]['$limit']=$options['limit'];
		$aggregate[]['$skip']= (int)$options['skip'];
		$aggregate[]['$sort'] = $options['sort'];
		
		$options = self::set_params($default, $params);
		$reports = $collection->aggregate($aggregate);
		
		if(!empty($reports)){
			$objs = [];
			foreach($reports as $report){
				$objs[] = self::instantiate($report);
			}
			return $objs;
		}else{
			return [];
		}
		
	}

	public static function get_posts($params=[]){
		$collection = self::get_collection(self::$collection);
		$default=[
			"filters"=>[ 'resolution' => ['$exists'=> false], 'type'=> self::TYPE_POST ],
			'limit' =>1,
			'skip' => 0,
			'sort' => ['_id' => 1],
		];
		
		$options = self::set_params($default, $params);
		
		// Replace _ids with object ids
		foreach($options['filters'] as $k=>$v){
			if($k == '_id'){
				$options['filters'][$k] = new \MongoDB\BSON\ObjectId($v);
			}
		}
			
		$aggregate[]['$match'] = $options['filters'];
		$aggregate[]['$project'] = [
			'issue'=>1,
			'issuers'=>1,
			'poster_id'=>1,
			'content_id'=>1,
			'type'=>1,
			'count'=>['$size'=>['$ifNull'=>['$issuers', [] ]] ],
			'resolved'=>1
		];
		$aggregate[]['$limit']=$options['limit'] + $options['skip'];
		$aggregate[]['$skip']= $options['skip'];
		$aggregate[]['$sort'] = $options['sort'];
		
		$cursor = $collection->aggregate($aggregate);
		$report = [];
		foreach($cursor as $report){
			$report = $report;
		}
		
		if(empty($report)) return [];
		
		$post = Post::find([
			'params'=>['_id'=> new \MongoDB\BSON\ObjectId($report->content_id)],
			'limit'=>1
		]);
		
		
		if($post){
			$user = User::find($report->poster_id);

			$report->user = $user->get_minimal();
			$report->user['posts'] = Post::find_by_user($user->id, true);
			$report->post = $post[0];

			return self::instantiate($report);
		}else{
			$report = self::instantiate($report);
			$report->mark_as_resolved(self::RESOLUTION_DELETED);
			return self::get_posts(['skip'=>$options['skip']++]);
		}
	}
	
	public function add_issuer($id){
		$collection = self::get_collection(self::$collection);
		
		$collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($id)],
			[
				'$addToSet'=>[
					'issuers'=>$this->issuer_id
				],
				'$unset'=>['resolution' => 'true']
			]										
		);
	}
	
	public function update(){
		$collection = self::get_collection(self::$collection);
		
		$update = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($this->id)],[
			'$set'=>[
				'issue'=>$this->issue,
				'issuers'=>$this->issuers,
				'content_id'=>$this->content_id,
				'type'=>$this->type,
				'resolution'=>$this->resolution
			]
		]);
		
		return $update->getMatchedCount() ? $update->getMatchedCount() : false ;
	}
	
	public function get_resolved(){
		return 
			(isset($this->resolved) && !empty($this->resolved)) ? $this->resolved : false;
	}
	
	public function get_status(){
		return 
			$this->status;
	}
	
	private function validate(){
		if(empty($this->issue) || empty($this->issuer_id) || empty($this->poster_id) ||empty($this->content_id) || empty($this->type)){
			throw new \Exception("Validation failed!");
		}
		
		if(!ctype_xdigit($this->issuer_id)){
			throw new \Exception("Invalid issuer ID!");
		}
		
		if(!ctype_xdigit($this->content_id)){
			throw new \Exception("Invalid content ID!");
		}
		
		if(!in_array($this->issue, [self::SPAM, self::PORNOGRAPHY, self::HATRED_BULLYING, self::SELF_HARM, self::VIOLENT, self::ILLEGAL_ACTIVITIES, self::DECEPTIVE, self::COPYRIGHT , self::I_DONT_LIKE_IT])){
			throw new \Exception("Invalid report reason!");
		}
		
		if(!in_array($this->type, [self::TYPE_COMMENT, self::TYPE_POST])){
			throw new \Exception("Invalid report type!");
		}
		return true;
	}
	
	private function mark_as_resolved($resolution){
		$this->resolution = [
			'resolution'=> $resolution,
			'timestamp'=>new \MongoDB\BSON\UTCDateTime()
		];
		
		return $this->update();
	}
	
	public function remove(){
		if($this->type === self::TYPE_POST){
			$post = Post::find(['params'=>['_id'=>$this->content_id]]);
			if(empty($post)) return false;
			
			$removed = $post[0]->remove();
			if($removed){
				foreach($this->issuers as $issuer_id){
					User::record_good_report($issuer_id, (string)$this->id);
				}
				return $this->mark_as_resolved(self::RESOLUTION_DELETED);
			}
		}elseif($this->type === self::TYPE_COMMENT){
			if(Comment::remove($this->content_id)){
				foreach($this->issuers as $issuer){
					User::record_good_report($issuer_id, (string)$this->id);
				}
				return $this->mark_as_resolved(self::RESOLUTION_DELETED);
			}
		}
	}
	
	public function allow(){
		if($this->type === self::TYPE_POST){
			foreach($this->issuers as $issuer_id){
				User::record_bad_report($issuer_id, (string)$this->id);
			}
			return $this->mark_as_resolved(self::RESOLUTION_ALLOWED);
		}elseif($this->type === self::TYPE_COMMENT){
			if(Post::remove($this->content_id)){
				foreach($this->issuers as $issuer){
					User::record_bad_report($issuer->id, (string)$this->id);
				}
				return $this->mark_as_resolved(self::RESOLUTION_DELETED);
			}
		}
	}
	
	protected static function instantiate($result){ 
		$obj = new self;
		foreach($result as $k=>$v){
			if($k == "_id" || $k == "id"){
				$k = "id"; $v = (string) $v;
				$obj->created = (new \MongoDB\BSON\ObjectId($v))->getTimestamp();
			}
			
			$obj->$k = $v;
		}
		return $obj;
	}
}

?>