<?php namespace MemeVibe;

require_once __DIR__ . "/../vendor/autoload.php";

use \Firebase\JWT\JWT;

class Config{
	
	const DB_HOST = "mongodb://localhost:27017";
	const SITE_ROOT = "/home/iki789/public_html/dopachan";
	const SITE_ROOT_URL = "https://dopachan.com";
	const FILE_ROOT_URL = "https://f1.dopachan.com";
	const DS = DIRECTORY_SEPARATOR;

	const FILE_STORAGE_PATH = self::SITE_ROOT .self::DS. "storage/files";
	const FILE_TMP_PATH = self::SITE_ROOT .self::DS.  "storage/tmp";
	const REACTIONS_PATH = self::SITE_ROOT . self::DS . 'storage/reactions' ;

	const FILE_STORAGE_URL = self::FILE_ROOT_URL . "/files";
	const FILE_TMP_URL = self::FILE_ROOT_URL . "/tmp";
	const REACTIONS_URL = self::FILE_ROOT_URL . '/reactions' ;
	
	const FB_APP_ID = "183294449062371";
	const FB_APP_SECRET = "8fb04b058adeb185183af1ec00470de7";
	
	const GOOG_CLIENT_ID = "275231217627-v3vrd6qjh3hl3l27vo9e9c3j1btjcq35.apps.googleusercontent.com";
	const GOOG_CLIENT_SECRET = "vfilukFU8ubtSBm9KvtbV9CD";
	const GOOG_PROJECT_ID = "dopachan-1530069209539";
	
	public static function JWT_ENCODE($sub){
		$secret = "@2DoUYOukNowDUgaNDAIeWaemaBuda!4";
		$key = md5(sha1($secret));
		
		if(is_array($sub)){ $sub = json_encode($sub); }
		
		$token = array(
			"iss" => "https://dopachan/",
			"aud" => "http://localhost:4200",
			"sub" => $sub,
			"exp" => strtotime("+2 month"),
			"iat" => time()
		);

		return $jwt = JWT::encode($token, $key);
	}
	
	public static function JWT_DECODE($jwt){
		$secret = "@2DoUYOukNowDUgaNDAIeWaemaBuda!4";
		$key = md5(sha1($secret));

		return $jwt = JWT::decode($jwt, $key, array('HS256'));
	}
		
	public static function contextual_time($small_ts, $large_ts=false) {
		if(!$large_ts) $large_ts = time();
		$n = $large_ts - $small_ts;
		if($n <= 1) return 'less than 1 second ago';
		if($n < (60)) return $n . ' seconds ago';
		if($n < (60*60)) { $minutes = round($n/60); return 'about ' . $minutes . ' minute' . ($minutes > 1 ? 's' : '') . ' ago'; }
		if($n < (60*60*16)) { $hours = round($n/(60*60)); return 'about ' . $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago'; }
		if($n < (time() - strtotime('yesterday'))) return 'yesterday';
		if($n < (60*60*24)) { $hours = round($n/(60*60)); return 'about ' . $hours . ' hour' . ($hours > 1 ? 's' : '') . ' ago'; }
		if($n < (60*60*24*6.5)) return 'about ' . round($n/(60*60*24)) . ' days ago';
		if($n < (time() - strtotime('last week'))) return 'last week';
		if(round($n/(60*60*24*7))  == 1) return 'about a week ago';
		if($n < (60*60*24*7*3.5)) return 'about ' . round($n/(60*60*24*7)) . ' weeks ago';
		if($n < (time() - strtotime('last month'))) return 'last month';
		if(round($n/(60*60*24*7*4))  == 1) return 'about a month ago';
		if($n < (60*60*24*7*4*11.5)) return 'about ' . round($n/(60*60*24*7*4)) . ' months ago';
		if($n < (time() - strtotime('last year'))) return 'last year';
		if(round($n/(60*60*24*7*52)) == 1) return 'about a year ago';
		if($n >= (60*60*24*7*4*12)) return 'about ' . round($n/(60*60*24*7*52)) . ' years ago'; 
		return false;
	}
}

?>