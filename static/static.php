<?php 
require_once __DIR__ . '..\..\vendor\autoload.php';
use \MemeVibe\Post as Post;

$post = Post::find(['params'=>['_id'=>'5b1bcf626925a32f6c004cbb'],'limit'=>1])[0];

$link = !empty($post) ? 'https://dopachan.com/post/'.$post->id : 'https://dopachan.com' ;
$title = !empty($post->title) ? $post->title : "Dopachan - Lets share it!";
$des = "Discover and share funny pics, videos, and memes found on the internet!";
$des = !empty($post->title) ? "Dopachan - " . $des : $des ;

$link_image = './link_image_1200x630.jpg';
if(isset($post) && $post->isVid){
	$link_image = $post->thumbnail ? $post->thumbnail : $link_image ; 
}else{
	$link_image = $post->source ? $post->source : $link_image ;
}
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
  <meta name="application-name" content="<?php echo $title ?>"/>
  <meta name="msapplication-TileColor" content="#F1C40F" />
  <meta name="msapplication-TileImage" content="./mstile-144x144.png" />

  <!-- Open Graph -->
  <meta property="og:url" content="<?php echo $link ?>">
  <meta property="og:image" content="<?php echo $link_image ?>">
  <meta property="og:description" content="<?php echo $des ?>">
  <meta property="og:title" content="<?php echo $title ?>">
  <meta property="og:site_name" content="Dopachan – Just share it!">
  <meta property="og:see_also" content="<?php echo $link ?>">

  <!-- Twitter -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:url" content="<?php echo $link ?>">
  <meta name="twitter:title" content="<?php echo $title ?>">
  <meta name="twitter:description" content="<?php echo $des ?>">
  <meta name="twitter:image" content="<?php echo $link_image ?>">

  <meta name="description" content="<?php echo $des ?>">
  <meta name="keywords" content="memes, meme, funny, hilarious, cats, dogs, anime, humor, gaming">
  <link rel="canonical" href="<?php echo $link ?>" />
	<title><?php $title ?></title>
</head>

<body>
	<?php
		header("Location: " . $link);
		die();
	?>
</body>
</html>
