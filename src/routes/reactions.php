<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/../../vendor/autoload.php';

use \MemeVibe\Reactions as Reactions;
use \MemeVibe\JWT as Jwt;

$app->get("/api/reactions", function(Request $request, Response $response){
	$reactions = Reactions::find();
	
	return $response->withJson($reactions);
});

$app->get("/api/reactions/default", function(Request $request, Response $response){
	return $response->withJson(Reactions::get_default());
});

$app->post("/api/reactions", function(Request $request, Response $response){
	$body = $request->getParsedBody();
	$files = $request->getUploadedFiles();
	try{
		$r = new Reactions();
		$r->name = !empty($body['keywords']) ? $body['keywords'] : '' ;
		// Convert File object to $_FILES array
		$file['tmp_name'] = $files['file']->file;
		$file['name'] = $files['file']->getClientFilename();
		$file['size'] = $files['file']->getSize();
		$r->create($file);
		
		return $response->withJson(['success'=>1]);
	}catch(Exception $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}
});

$app->post("/api/reactions/{id}/default", function(Request $request, Response $response, Array $args){
	$id = isset($args['id']) && ctype_xdigit($args['id']) ? $args['id'] : '' ;
	if(empty($id)) return false;
	
	$reaction = new Reactions($args['id']);
	$reaction->mark_default();
	
});

$app->post("/api/reactions/{id}/default/remove", function(Request $request, Response $response, Array $args){
	$id = isset($args['id']) && ctype_xdigit($args['id']) ? $args['id'] : '' ;
	if(empty($id)) return false;
	
	$reaction = new Reactions($args['id']);
	$reaction->remove_default();
	
});

$app->delete("/api/reactions/{id}", function(Request $request, Response $response, Array $args){
	$id = isset($args['id']) && ctype_xdigit($args['id']) ? $args['id'] : '' ;
	if(empty($id)) return;
	$reaction = new Reactions($id);
	$reaction->remove();
	
	return $response->getBody()->write('');
});

$app->put("/api/reactions/position", function(Request $request, Response $response, Array $args){
	$body = $request->getParsedBody();
	if(empty($body['reactions'])) return;
	
	foreach($body['reactions'] as $reaction){
		$r = new Reactions($reaction['id']);
		$r->position = $reaction['position'];
		$r->update_position();
	}
	
	return $response->getBody()->write('');
});


?>