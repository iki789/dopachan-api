<?php 

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once __DIR__ . '/../../vendor/autoload.php';

use \MemeVibe\User as User;
use \MemeVibe\Config as Config;
use \MemeVibe\Files as Files;
use \MemeVibe\Post as Post;
use \MemeVibe\Activities as Activities;
use \MemeVibe\JWT as JWT;

$app->get('/api/users/{id}', function(Request $request, Response $response, Array $args){
	$id = $args['id'];
	$user = User::find($id);
	if($user){
		$user = $user->get_minimal();	
		return $response->withJson($user);
	}else{
		return $response->write("")->withStatus(404);
	}
});

$app->get('/api/users', function(Request $request, Response $response){
	$jwt = new Jwt();
	if(empty($jwt->token)){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	$user = $jwt->get_user();
	if($user['group'] < User::GROUP_ADMIN){
		return $response->withJson(['error'=>'Unauthorized'])->withStatus(401);
	}
	
	$query = $request->getQueryParams();
	$sort = isset($query['sort']) ? $query['sort'] : ['_id'=>-1];
	$limit = isset($query['limit']) ? (int)$query['limit'] : 50;
	$skip = isset($query['skip']) ? (int)$query['skip'] : 0 ;
	$search = isset($query['q']) ? $query['q'] : null ;
	$full_admin = isset($query['full-admins']) && $query['full-admins'] == 'true' ? User::GROUP_FULL_ADMIN : false ;
	$senior_admin = isset($query['senior-admins']) && $query['senior-admins'] == 'true' ? User::GROUP_SENIOR_ADMIN : false ;
	$admin = isset($query['admins']) && $query['admins'] == 'true'? User::GROUP_ADMIN : false ;
	$mod = isset($query['moderators']) && $query['moderators'] == 'true' ? User::GROUP_MODERATORS : false ;
	$user = isset($query['users']) && $query['users'] == 'true' ? User::GROUP_USER : false ;
	$filters = [];
	
	if(isset($query['sort']) && is_string($query['sort'])){
		$sort_array = explode('-', $query['sort']);
		if(count($sort_array) == 2){
			$by = $sort_array[0];
			$order = $sort_array[1];

			$allowed_sorts = ['email', 'posts', 'group', 'date'];

			if(!in_array($by, $allowed_sorts)){ $by = '_id'; }
			if($by == 'date'){ $by = '_id'; }
			if($order == 'asc'){ $order = -1; }
			elseif($order == 'desc'){ $order = 1; }
			else{ $order = 1; }

			$sort = [$by=>$order];		
		}else{
			$sort = ['_id'=>-1];
		}
	}
	
	$admin_filters = array_values(array_filter([$full_admin, $senior_admin, $admin, $mod, $user]));
	
	if(!empty($admin_filters) && is_array($admin_filters)){
		$filters['group'] = ['$in'=>$admin_filters];	
	}
	
	if(!empty($search)){ $filters['search'] = $search; }
	
	$users = User::find_many([
		'filters'=>$filters,
		'sort'=>$sort,
		'limit'=>$limit,
		'skip'=>$skip
	]);
	
	$user_ids = [];
	foreach($users as $user){
		$user_ids[] = $user->id;
	}
	
	$post_counts = Post::get_posts_count($user_ids);
	foreach($users as $user){
		$user->role = $user->get_group();
		$user->post_count = isset($post_counts[$user->id]) ? $post_counts[$user->id] : 0;
	}
	
	return $response->withJson($users);
});

$app->get('/api/users/current/', function(Request $request, Response $response){
	$jwt = new Jwt();
	
	if(empty($jwt->token)){
		return $response->withStatus(401);	
	}
	$user = $jwt->get_user();

	if($user['account_type'] === User::ACCOUNT_TYPE_FB){
		$user = User::find($user['fb_id'], $user['account_type']);
	}elseif($user['account_type'] === User::ACCOUNT_TYPE_GOOG){
		$user = User::find($user['goog_id'], $user['account_type']);
	}elseif($user['account_type'] === User::ACCOUNT_TYPE_LOCAL){
		$user = User::find($user['id'], $user['account_type']);
	}

	if(!$user){
		return $response->withJson("")->withStatus(404);
	}

	$to_return = [
		"id"=>$user->id,
		"name"=>$user->name,
		"display_name"=>$user->display_name,
		"avatar"=>$user->avatar,
		"account_type"=>$user->account_type,
	];
	!empty($user->email) ? $to_return['email'] =  $user->email : null ;
	!empty($user->gender) ? $to_return['gender'] =  $user->gender : null ;
	!empty($user->about) ? $to_return['about'] =  $user->about : null ;
	!empty($user->fb_id) ? $to_return['fb_id'] =  $user->fb_id : null ;
	!empty($user->dob) ? $to_return['dob'] =  $user->dob : null ;

	if(!empty($user->location)){
		if(!empty($user->loaction->city)){
			$to_return['location']['city'] = $user->location->city;
		}

		if(!empty($user->location->country)){
			$to_return['location']['country'] = $user->location->country;
		}
	}

	return $response->withJson($to_return);
});

$app->post('/api/users/register', function(Request $request, Response $response){
	$data = $request->getParsedBody();
	if(isset($data['user']['email'])){
		$user['name'] = $data['user']['name'];
		$user['email'] = $data['user']['email'];
		$user['password'] = $data['user']['password'];
		
		try{
			if($user = User::create(User::ACCOUNT_TYPE_LOCAL, "", $user)){
				$token = JWT::encode(['id'=>$user->id, 'account_type'=>$user->account_type, 'avatar'=>$user->avatar, 'display_name'=>$user->display_name, 'group'=>1]);
				$to_return = [
					'success' => 1,
					'token' => $token
				];
				$response->getBody()->write(json_encode($to_return));
			}else{
				$response->getBody()->write(json_encode(['success'=>0]));
			}
		}catch(\Exception $e){
			$response->getBody()->write(json_encode(['success'=>0, 'error'=>$e->getMessage()]));
		}
	}else{
		$response->getBody()->write(json_encode(['success'=>0, 'error'=>'Email required']));
	}
	
});

$app->post('/api/users/login/fb', function(Request $request, Response $response){
	$body = $request->getParsedBody();
	
	if(isset($body['token']) && !empty($body['token'])){
		try{
			if($user = User::create(User::ACCOUNT_TYPE_FB, $body['token'])){
				if($user->banned){
					return $response->withJson(['success'=>0, 'error'=>'banned']);
				}
				$to_return = [
					'success'=>1, 
					'token'=>Config::JWT_ENCODE([
						'id' => $user['id'],
						'fb_id' => $user['fb_id'],
						'avatar' => $user['avatar'],
						'display_name' => $user['display_name'],
						'account_type' => $user['account_type'],
						'group'=> $user['group'],
						'age_group'=>$user['age_group']
					]),
				];
				
				return $response->withJson($to_return);
			}	
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
});
	
$app->post('/api/users/login', function(Request $request, Response $response){
	$body = $request->getParsedBody();
	$email = $body['email'];
	$password = $body['password'];
	
	if($user = User::authenticate($email, $password)){
		if($user->banned){
			return $response->withJson(['success'=>0, 'error'=>'banned']);
		}
		$token = JWT::encode([
			'id'=>$user->id, 
			'account_type'=>$user->account_type, 
			'avatar'=>$user->avatar, 
			'display_name'=>$user->display_name,
			'group'=>$user->get_group(),
			'age_group'=>$user->age_group
		]);
		$to_return = ['success'=>1,'token'=>$token];
		return $response->withJson($to_return);
	}else{
		return $response->withJson(['success'=>0]);
	}
});

$app->post('/api/users/login/google', function(Request $request, Response $response){
	$body = $request->getParsedBody();
	$token = $body['token'] ? $body['token'] : "" ;
	
	if(!empty($token)){
		try{
			if($user = User::create(User::ACCOUNT_TYPE_GOOG, $token)){
				if($user->banned){
					return $response->withJson(['success'=>0, 'error'=>'banned']);
				}
				$to_return = [
					'success'=>1, 
					'token'=>Config::JWT_ENCODE([
						'id' => $user['id'],
						'goog_id' => $user['goog_id'],
						'avatar' => $user['avatar'],
						'display_name' => $user['display_name'],
						'account_type' => $user['account_type'],
						'group'=>$user['group'],
						'age_group'=>$user->age_group
					]),
				];
				
				return $response->withJson($to_return);
			}	
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
});

$app->put('/api/users', function(Request $request, Response $response){
	if(!$jwt = new JWT()){
		return $response->withStatus(401);	
	}
	
	$to_put = $request->getParsedBody();
	// Remove undefined null values
	$to_remove = [];

	$x = $jwt->decode();
	$user_jwt = json_decode($x->sub, true);
	
	$user = new User();
	$user->id = $user_jwt['id'];
	$user->account_type = $user_jwt['account_type'];
	
	try{
		if($user->update($to_put, $to_remove)){
			// Return a token
			$data = [
				'id'=>$user->id, 
				'account_type'=>$user->account_type, 
				'avatar'=>$to_put['avatar'], 
				'display_name'=>$to_put['display_name'],
				'group'=>$user_jwt['group']
			];
			if(isset($user_jwt['fb_id'])){ $data['fb_id'] = $user_jwt['fb_id']; }
			if(isset($user_jwt['goog_id'])){ $data['goog_id'] = $user_jwt['goog_id']; }
			$token = JWT::encode($data);
			
			return $response->withJson(['success'=>1, 'token'=>$token]);
		}
	}catch(Exception $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}
});

$app->post('/api/users/avatar', function(Request $request, Response $response){
	
	if(!$jwt = new \MemeVibe\JWT()){
		$response->withStatus(401);	
	}
	
	$files = $request->getUploadedFiles();

	// Create a $_FILES array to compensate with API
	$uploaded_file['tmp_name'] = $files['file']->file;
	$uploaded_file['name'] = $files['file']->getClientFilename();
	$uploaded_file['size'] = $files['file']->getSize();
	$uploaded_file['error'] = $files['file']->getError();
	$uploaded_file['type'] = $files['file']->getClientMediaType();

	if ($files['file']->getError() === UPLOAD_ERR_OK) {
		$file_sys = new Files();
		$file_tmp_url = $file_sys->upload_to_tmp($uploaded_file);
		$file_name = basename($file_tmp_url);
		$res = $file_sys->move_from_tmp($file_name);
		if($res){
			$response->getBody()->write(json_encode(["success"=>1, "path"=>$file_sys->path_to_url($res)]));
		}else{
			$response->getBody()->write(json_encode(["success"=>0, "error"=>$res->getMessage()]));
		}	
	}else{
		$response->getBody()->write(json_encode(["success"=>0, "error"=> $uploaded_file->getError()]));
	}
});

$app->get('/api/users/collection/{id}', function(Request $request, Response $response, Array $args){
	$query_params = $request->getQueryParams();
	$skip = isset($query_params['skip']) ? (int)$query_params['skip'] : 0;
	$limit = isset($query_params['limit']) ? (int)$query_params['limit'] : 10;
	$get_nsfw = false;
	
	$jwt = new Jwt();
	if($jwt->token){
		$user = $jwt->get_user();
		if($user['id'] == $args['id'] || (isset($user['age_group']) && $user['age_group'] > 17) || $user['group'] > 60 ){
			$get_nsfw = true;
		}
	}
	
	try{
		$posts = (new User($args['id']))->get_saved_posts($skip, $limit, $get_nsfw, (isset($user['id']) ? $user['id'] : ''));
		
		return $response->withJson($posts);
	}catch(\MongoDB\Driver\Exception\InvalidArgumentException $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}catch(Exception $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}
	
	$posts = (new User($user_id))->get_saved_posts($skip, $limit);
	
	return $response->withJson($posts);
});

$app->get('/api/users/posts/{id}', function(Request $request, Response $response, Array $args){
	$query_parmas = $request->getQueryParams();
	$options = ['params'=>['user_id'=>$args['id'], 'under_review'=>null], 'sort'=>['_id'=>-1], 'limit'=>10, 'skip'=>(int)$query_parmas['skip']];
	$get_nsfw = false;
	
	$jwt = new Jwt();
	if($jwt->token){
		$user = $jwt->get_user();
		if($user['id'] == $args['id'] || (isset($user['age_group']) && $user['age_group'] > 17) || $user['group'] > 60 ){
			$get_nsfw = true;
		}

		if($user['id'] == $args['id'] || $user['group'] > 60 ){
			unset($options['params']['under_review']);
		}
	}
	
	try{
		$posts = Post::find($options, $args['id'], $get_nsfw, (isset($user['id']) ? $user['id'] : ""));
		
		return $response->withJson($posts);
	}catch(\MongoDB\Driver\Exception\InvalidArgumentException $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}catch(Exception $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}
});

$app->get('/api/users/recent/{id}', function(Request $request, Response $response, Array $args){
	$query_params = $request->getQueryParams();
	$skip = isset($query_params['skip']) ? (int)$query_params['skip'] : 0;
	$options = ['skip'=>$skip	];
	$options['filters'] = ['user_id'=>$args['id']];
	$get_nsfw = false;
	$jwt = new Jwt();
	if($jwt->token){
		$user = $jwt->get_user();
		if($user['id'] == $args['id'] || (isset($user['age_group']) && $user['age_group'] > 17) || $user['group'] > 60 ){
			$get_nsfw = true;
		}
	}
	try{
		$activities = Activities::find_many($options);
		$post_ids = [];
		foreach($activities as $act){
			$post_ids[] = new \MongoDB\BSON\ObjectId($act->post_id);
		}

		$posts = Post::find(['params'=>['_id'=>['$in'=>$post_ids]]], $args['id'], $get_nsfw, (isset($user['id']) ? $user['id'] : ""));
		foreach($posts as $post){
			foreach($activities as $act){
				if($act->post_id == $post->id){
					$post->type = $act->type;
				}
			}
		}
		return $response->withJson($posts);
			
		
	}catch(\MongoDB\Driver\Exception\InvalidArgumentException $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}catch(Exception $e){
		return $response->withJson(['error'=>$e->getMessage()]);
	}
});
	
?>