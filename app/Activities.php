<?php namespace MemeVibe;

require_once __DIR__ . "/../vendor/autoload.php";

use MemeVibe\Base as Base;

class Activities extends Base{
	
	public $id;
	public $user_id;
	public $post_id;
	public $comment_id;
	public $type;
	public $comments;
	const TYPE_POST = 0;
	const TYPE_COMMENT = 1;
	const TYPE_REACT = 2;
	private static $collection = "Activities";
	
	public function __construct(){ }
	
	public static function find($id){
	
		if(!ctype_xdigit($id)){
			throw new \Exception("Invalid ID");
		}
		
		$collection = self::get_collection(self::$collection);
		$document = $collection->findOne(['_id'=>new \MongoDB\BSON\ObjectId($id)]);
		if($document){
			return self::instantiate($document);
		}
		return false;
	}
	
	public static function find_many($options=[]){
		$collection = self::get_collection(self::$collection);
		$default = [
			'filters'=>[],
			'limit'=>20,
			'skip'=>0,
			'sort'=>['_id'=>-1]
		];
		
		$options = self::set_params($default, $options);
		
		foreach($options['filters'] as $k=>$v){
			if($k == '_id'){
				$options['filters'][$k] = new \MongoDB\BSON\ObjectId($v);
			}
		}
		
		$cursor = $collection->find($options['filters'],[
			'limit'=>$options['limit'],
			'skip'=>$options['skip'],
			'sort'=>$options['sort']
		]);
		
		$activities = [];
		foreach($cursor as $activity){
			$activities[] = self::instantiate($activity);
		}
		return $activities;
	}
	
	public static function create(Array $params=[]){
		$collection = self::get_collection(self::$collection);
		$user_id = isset($params['user_id']) ? (string)$params['user_id'] : "" ;
		$post_id = isset($params['post_id']) ? (string)$params['post_id'] : "" ;
		$comment_id = isset($params['comment_id']) ? (string)$params['comment_id'] : "" ;
		$type = isset($params['type']) ? (int)$params['type'] : "" ;
		
		if(!self::validate($params)) return false;
		$ops = [
			'$set'=>[
				'user_id' => $user_id, 
				'post_id' => $post_id,
			],
			'$addToSet' => ['type'=>$type]
		];
		if($type == self::TYPE_COMMENT && !empty($comment_id)){ $ops['$push'] = ['comments'=> $comment_id]; }
		$query = $collection->updateOne(
			['user_id' => $user_id, 'post_id' => $post_id],
			$ops,
			['upsert'=>true]
		);
		
		if($query){
			return $query->getUpsertedId();
		}else{
			return false;
		}
	}
	
	public static function validate($fields){
		$whitelist = array_keys((get_object_vars(new Activities)));
		foreach($fields as $field=>$value){
			if(!in_array($field, $whitelist)){
				throw new \Exception("Invalid field");
				break;
			}
			if($field == 'user_id' || $field == 'post_id'){
				if(!ctype_xdigit($value)){
					throw new \Exception("Invalid $field field value '$value'");
					break;
				}
			}
			if($field == 'type' && !in_array($value, [self::TYPE_POST, self::TYPE_COMMENT, self::TYPE_REACT])){
				if(!ctype_xdigit($value)){
					throw new \Exception("Not a valid type '$value'");
					break;
				}
			}
		}
		return true;
	}

	public static function remove($id='', $activity, $post_id='', $comment_id=''){
		$collection = self::get_collection(self::$collection);
		if(!empty($id)){
			$act = self::find($id);
		}elseif(!empty($post_id)){
			$act = (self::find_many(['filters'=>['post_id'=>(string)$post_id]]))[0];
		}elseif(!empty($comment_id)){
			$act = (self::find_many(['filters'=>['comments'=>(string)$comment_id]]))[0];
		}else{
			return false;
		}
				
		if(!$act) return false;
		
		if(count($act->type) == 1 && in_array($activity, (array)$act->type) && $activity != self::TYPE_COMMENT){
			return self::remove_all((string)$act->id);
		}
		
		$ops = [ '$pull'=>['type'=>$activity] ];
		
		if($activity == self::TYPE_COMMENT){
			if(count($act->comments) == 1 && count($act->type) == 1){
				return self::remove_all((string)$act->id);
			}
			$ops['$pull']['comments'] = $comment_id;
		}
		
		$update = $collection->updateOne(
			['_id'=> new \MongoDB\BSON\ObjectId($act->id)],
			$ops
		);
		return $update->getMatchedCount() > 0 ? true : false;
	}
	
	public static function remove_all($id){
		$collection = self::get_collection(self::$collection);
		
		$result = $collection->deleteOne(['_id'=> new \MongoDB\BSON\ObjectId($id)]);
		
		return $result->getDeletedCount() > 0 ? true : false;
	}
	
}

//$params = [
//	'user_id' => "5af224116925a32ef0000cc9",
//	'post_id' => "5abe070e6925a308340032b4",
//	'type' => 2
//];
//echo "<pre>";
//print_r(Activities::find_many(['filters'=>['comments'=>(string)"5b76c8a06925a305a00037df"]]));

?>