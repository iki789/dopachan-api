<?php 
namespace MemeVibe;

require_once __DIR__ . '/../vendor/autoload.php';

use Facebook\Facebook, FacebookApp, FacebookRequest;
use MemeVibe\Base as Base;
use MemeVibe\Files as Files;
use MemeVibe\Post as Post;

class User extends Base{
	
	public $id;
	public $name;
	public $display_name;
	public $avatar;
	public $about;
	public $email;
	public $password;
	public $new_password;
	public $location;
	public $gender;
	public $dob;
	public $age_group;
	public $fb_id;
	public $goog_id;
	public $strikes;
	public $stats;
	public $banned;
	public $account_type;
	public $access_token;
	public $created;
	private $group;
	static private $collection = "Users";
	
	const ACCOUNT_TYPE_LOCAL = 0;
	const ACCOUNT_TYPE_FB = 1;
	const ACCOUNT_TYPE_GOOG = 2;
	
	const GROUP_USER = 1;
	const GROUP_MODERATORS = 20;
	const GROUP_ADMIN = 60;
	const GROUP_SENIOR_ADMIN = 80;
	const GROUP_FULL_ADMIN = 100;
		
	public function __construct($id=''){ 
		$this->id = $id;
	}
	
	public static function create_fb_user($access_token){
		
		$fb = new Facebook([
			'app_id' => Config::FB_APP_ID,
			'app_secret' => Config::FB_APP_SECRET,
			'default_graph_version' => 'v3.0',
		]);

		try{
			if(empty($access_token)){
				throw new \Exception('No cookie set or no OAuth data could be obtained from cookie.');
				exit;
			}
			
			// Returns a `Facebook\FacebookResponse` object
			try{
				$response = $fb->get('/me?fields=id,first_name,last_name,email,picture', $access_token);
			}
			catch(Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			
			// Put fields to database
			$fb_user = $response->getGraphUser();
			
		}catch(Facebook\Exceptions\FacebookResponseException $e){
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		$user =[
			'name' => $fb_user['first_name'] . " " .$fb_user['last_name'],
			'display_name' => $fb_user['first_name'] . " " .$fb_user['last_name'],
			'account_type'=>self::ACCOUNT_TYPE_FB,
			'fb_id'=>$fb_user['id'],
			'group'=>self::GROUP_USER,
			'age_group'=>13
		];
		
		if(isset($fb_user['gender'])){
			$user['gender'] = $fb_user['gender'] == "female" ? 0 : 1;
		}
		
		if(isset($fb_user['age_range']['min'])){
			$user['age_group'] = $fb_user['age_range']['min'];	
		}
		
		if(isset($fb_user['picture']['url'])){
			$path = self::save_avatar($fb_user['picture']['url']);
			$fs = new Files();
			$user['avatar'] = $fs->path_to_url($path);
		}
		
		if(isset($fb_user['email'])){
			$user['email'] = $fb_user['email'];
		}
		
		if(isset($fb_user['location'])){
			if(isset($fb_user['location']['location']['country'])){ 
				$user['location']['country'] = $fb_user['location']['location']['country'];
			}
			
			if(isset($fb_user['location']['location']['city'])){
				$user['location']['city'] = $fb_user['location']['location']['city'];
			}
		}
		
		if($result = self::find($fb_user['id'], self::ACCOUNT_TYPE_FB)){
			// Just log them in with a long-living token
			$user['access_token'] = self::get_fb_token($access_token);
			$user['id'] = $result->id;
			$user['avatar'] = $result->avatar;
			$user['display_name'] = $result->display_name;
			$user['fb_id'] = $result->fb_id;
			$user['account_type'] = $result->account_type;
			$user['group'] = $result->get_group();
			
			return $user;
		}
		
		// Insert into DB
		$user['token'] = self::get_fb_token($access_token);
		$user['id'] = (string)self::put_FB_GG_user_in_db($user);
		return $user;
	}
	
	public static function create_goog_user($access_token){
		if(empty($access_token)){
			throw new \Exception("Invalid token");
		}
		$client = new \Google_Client(['client_id' => Config::GOOG_CLIENT_ID]);
		$payload = $client->verifyIdToken($access_token);
		if ($payload) {
			// Is a valid token
			$user = [
				'name'=> $payload['name'],
				'display_name'=> $payload['name'],
				'email'=> $payload['email'],
				'goog_id'=> $payload['sub'],
				'account_type'=> self::ACCOUNT_TYPE_GOOG,
				'group'=> self::GROUP_USER,
				'age_group'=> '13'
			];
			
			if(isset($payload['picture'])){
				$path = self::save_avatar($payload['picture']);
				$fs = new Files();
				$user['avatar'] = $fs->path_to_url($path);
			}
			
			
			if($result = self::find($user['goog_id'], self::ACCOUNT_TYPE_GOOG)){
				// Just log them in
				$user['id'] = $result->id;
				$user['avatar'] = $result->avatar;
				$user['display_name'] = $result->display_name;
				$user['goog_id'] = $result->goog_id;
				$user['account_type'] = $result->account_type;
				$user['group'] = $result->get_group();

				return $user;
			}
			
			// Insert into DB
			$user['id'] = (string)self::put_FB_GG_user_in_db($user);
			return $user;
			
		}else{
			throw new \Exception("Invalid token");
		}
	}
	
	private static function put_FB_GG_user_in_db($user){
		$collection = self::get_collection(self::$collection);
		$result = $collection->insertOne($user);
		if($result){
			return $result->getInsertedId();
		}else{
			return false;
		}
	}
	
	public static function find($id, $account_type=""){
		$collection = self::get_collection(self::$collection);
		
		if(empty($account_type)){
			if(!ctype_xdigit($id) && strlen($id) != "24"){ 
				throw new \Exception("Invalid ID");
			}
			$user = $collection->findOne([
				'_id'=> new \MongoDB\BSON\ObjectId($id)
			]);
		}
		
		if($account_type === self::ACCOUNT_TYPE_LOCAL){
			if(!ctype_xdigit($id) && strlen($id) != "24"){ return "false"; }
			try{
				$user = $collection->findOne([
					'_id'=> new \MongoDB\BSON\ObjectId($id),
					'account_type'=> $account_type
				]);	
			}catch(Exeption $e){
				echo $e->getMessage();
			}
			if(!$user){ return false; }
		}
		
		if($account_type === self::ACCOUNT_TYPE_FB){
			try{
				$user = $collection->findOne([
					// Fb ids are not BSON
					'fb_id'=> $id,
					'account_type'=> $account_type
				]);	
			}catch(Exeption $e){
				echo $e->getMessage();
			}
		}
		
		if($account_type === self::ACCOUNT_TYPE_GOOG){
			try{
				$user = $collection->findOne([
					// goog ids are not BSON
					'goog_id'=> $id,
					'account_type'=> $account_type
				]);	
			}catch(Exeption $e){
				echo $e->getMessage();
			}
		}
		
		if($user){
			$obj = self::instantiate($user);
			return $obj;
		}else{
			return false;
		}
	}
	
	public static function find_many($options=[]){
		$collection = self::get_collection(self::$collection);
		
		$default=[
			'filters'=>[],
			'fields'=>['display_name', 'email', 'group', 'banned', 'deactivated'],
			'limit' => 10,
			'skip' => 0,
			'sort' => ['_id' => 1],
		];
		
		$options = self::set_params($default, $options);
		
		foreach($options['fields'] as $field){
			$options['fields'][$field] = true;
		}
		
		if(isset($options['filters']['search'])){
			$options['filters']['$text'] = ['$search'=>$options['filters']['search'] ];
			unset($options['filters']['search']);
		}
		
		$users = $collection->find(
			$options['filters'],
			[
			'projection'=>$options['fields'],
			'limit'=>$options['limit'],
			'sort'=>$options['sort'],
			'skip'=>$options['skip']
		]);
		
		$user_objs = [];
		foreach($users as $user){
			$user_objs[] = self::instantiate($user);
		}
		return $user_objs;
	}
	
  public static function create($account_type, $access_token, $user=""){
		// Create Local Account
		
		if($account_type == self::ACCOUNT_TYPE_LOCAL && 
			 filter_var($user['email'], FILTER_VALIDATE_EMAIL) &&
			 (isset($user['password']) && !empty($user['password']) && strlen($user['password']) > 5) &&
			 (isset($user['name']) && !empty($user['name']) && strlen($user['name']) > 2)
			){
			
			if(!self::user_exists($user['email'])){
				$obj = new self();
				$obj->created = $user['created'] = new \MongoDB\BSON\UTCDateTime;
				$obj->password = $user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);
				$obj->name = $user['name'] = $user['name'];
				$obj->display_name = $user['display_name'] = $user['name'];
				$obj->avatar = $user['avatar'] = "./assets/avatars/". rand(0,114) . ".jpg";
				$obj->account_type = $user['account_type'] = self::ACCOUNT_TYPE_LOCAL;
				$obj->group = $user['group'] = self::GROUP_USER;
				$obj->age_group = 13;
				$collection = self::get_collection(self::$collection);
				$insert = $collection->insertOne($user);
				if($obj->id = (string)$insert->getInsertedId()){
					return $obj;
				}else{
					throw new \Exception("Failed putting data into database");	
				}
			}else{
				throw new \Exception("User already exists");
			}	

		}
		
		if($account_type == self::ACCOUNT_TYPE_FB){
			if(isset($access_token) && !empty($access_token)){
				return self::create_fb_user($access_token);
			}else{
				throw new \Exception("Invalid access token");
				return false;
			}
		} 
		
		if($account_type == self::ACCOUNT_TYPE_GOOG){
			if(isset($access_token) && !empty($access_token)){
				return self::create_goog_user($access_token);
			}else{
				throw new \Exception("Invalid access token");
			}
		}
	}
	
	public function ban($admin_id, $admin_group, $reason){
		if(!ctype_xdigit($admin_id)){ throw new \Exception("Invalid admin_id"); }
		if($admin_group < self::GROUP_ADMIN){ throw new \Exception("Unauthrized to perform action"); }

		$collection = self::get_collection(self::$collection);
		$update = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($this->id)], ['$addToSet'=>['banned'=>['admin'=> $admin_id, 'reason'=> $reason]]]);
		
		return $update->getMatchedCount() ? $update->getMatchedCount() : false ; 
	}
	
	public function strike($admin_id, $admin_group, $reason){
		$collection = self::get_collection(self::$collection);
		if($admin_group < self::GROUP_ADMIN) return false;
		$update = $collection->updateOne(['_id'=> new \MongoDB\BSON\ObjectId($this->id) ],['$addToSet'=>['strikes'=>[ 'admin'=> $admin_id, 'reason'=> $reason]]]);
		
		return $update->getMatchedCount() ? $update->getMatchedCount() : false ; 
	}
	
	public function delete(){
		if(strlen($this->id) == 24 && ctype_xdigit($this->id)){
			$this->collection->deleteOne([ '_id' => new \MongoDB\BSON\ObjectId($this->id)]);
			return true;	
		}
	}
	
	public function add_to_collection($post_id){
		$collection = self::get_collections_collection();
		
		$result = $collection->updateOne(
			['user_id'=>$this->id, 'post_id'=>$post_id],
			['$set'=>['user_id'=>$this->id, 'post_id'=>$post_id]],
			['upsert'=>true]
		);
		if($result->getUpsertedCount() > 0 || $result->getMatchedCount() > 0){
			$collection = Post::get_collection(Post::$collection);
			$result = $collection->updateOne([
				'_id'=> new \MongoDB\BSON\ObjectId($post_id)
			],[
				'$addToSet'=>[
					'saved'=>$this->id
				]
			]);
		}
		
		return $result->getMatchedCount() > 0 ? true : false ;
	}
	
	public function get_saved_posts($skip=0, $limit=10, $nsfw=flase, $viewer_id=""){
		$collection = self::get_collections_collection();
		$result = $collection->find(['user_id'=>$this->id],['skip'=>(int)$skip, 'limit'=>(int)$limit]);

		$post_ids = [];
		foreach($result as $post){
			$post_ids[] = new \MongoDB\BSON\ObjectId($post->post_id);
		}
		$posts = Post::find([
			'params'=>[ '_id'=>['$in'=>$post_ids] ],
			'limit'=>(int)$limit
		],$this->id, $nsfw, $viewer_id);
		return $posts;
	}
	
	public function remove_from_collection($post_id){
		$collection = "Collections";
		$collection = self::get_collection($collection);
		
		$delete = $collection->deleteMany(['user_id'=>$this->id, 'post_id'=>$post_id]);
		
		$collection = Post::get_collection(Post::$collection);
		$update = $collection->updateOne([
			'_id'=> new \MongoDB\BSON\ObjectId($post_id)
		],[
			'$pull'=>[
				'saved'=>$this->id
			]
		]);
		
		return true;
	}
	
	private static function get_collections_collection(){
		$collection = "Collections";
		return $collection = self::get_collection($collection);
	}
	
	public static function save_avatar($link){
		$path = Config::FILE_TMP_PATH;
		$file['name']= "asd.jpg";
		$file['tmp_name'] = $path . Config::DS . $file['name'];
		$file_content = file_put_contents($file['tmp_name'], file_get_contents($link));
		$file['size'] = filesize($file['tmp_name']);
		$fs = new Files();
		if($tmp_path = $fs->upload_to_tmp($file)){
			$result = $fs->move_from_tmp(basename($tmp_path));
			if($result){
				return $result;
			}else{
				return false;
			}
		}else{
			throw new \Exception("Failed moving to temp");
		}
	}
	
	public function get_minimal(){
		$to_return = [
			'id'=>$this->id,
			'about'=> $this->about,
			'avatar'=> $this->avatar,
			'display_name'=> $this->display_name,
			'strikes'=>$this->strikes
		];
		
		return $to_return;
	}
	
	public function get_post_count(){
		return 
			$count = Post::find_by_user($this->id, true);
	}
	
	public function update($to_update_fields=[], $to_remove_fields = []){
		if(empty($to_remove_fields) && empty($to_update_fields)) return false;
		if(!$this->id || !ctype_xdigit($this->id) || strlen($this->id) != 24){ 
			throw new \Exception("Invalid user ID"); 
		}
		
		// Remove undefined and id field
		foreach($to_update_fields as $k=>$v){
			if($k == "id"){
				// Remove user id
				unset($to_update_fields[$k]);
			}
			
			// Remove same values
			if($k == 'avatar'){
				if($v == $this->avatar){
					unset($to_update_fields[$k]);
				}
			}
			// Check if location is empty
			if($k == "location"){
				if(!isset($to_update_fields[$k]['country'])){
					unset($to_update_fields[$k]);
				}
			}
			// Removed undefined values
			if($v == "undefined" || $v == ""){
				$to_update_fields[$k] = "1";
				unset($to_update_fields[$k]);
			}
		}
		
		foreach($to_remove_fields as $f=>$v){
			// Unset fields which can not be removed e.g password, email
			if($f == "_id"){ unset($to_remove_fields[$f]); }
			if($f == "password"){ unset($to_remove_fields[$f]); }
			if($f == "email"){ unset($to_remove_fields[$f]); }
			if($f == "name"){ unset($to_remove_fields[$f]); }
			if($f == "display_name"){ unset($to_remove_fields[$f]); }
			if($f == "avatar"){ unset($to_remove_fields[$f]); }
			if($f == "account_type"){ unset($to_remove_fields[$f]); }
			if($f == "fb_id"){ unset($to_remove_fields[$f]); }
			if($f == "goog_id"){ unset($to_remove_fields[$f]); }
		}
		if($this->validate_fields($to_update_fields)){
			if(isset($to_update_fields['new_password']) || isset($to_update_fields['password']) || isset($to_update_fields['email'])){
				if($this->account_type == self::ACCOUNT_TYPE_LOCAL){
					if(!isset($to_update_fields['password']) && empty($to_update_fields['password'])){
						throw new \Exception("Password can not be empty");
					}
					if(!isset($to_update_fields['new_password']) && empty($to_update_fields['new_password'])){
						throw new \Exception("New password can not be empty");
					}
					if(self::authenticate($to_update_fields['email'], $to_update_fields['password'])){
						if(isset($to_update_fields['new_password'])){
							$to_update_fields['password'] = password_hash($to_update_fields['new_password'], PASSWORD_BCRYPT);
							unset($to_update_fields['new_password']);
						}else{
							$to_update_fields['password'] = password_hash($to_update_fields['password'], PASSWORD_BCRYPT);
						}
					}else{
						throw new \Exception("Invalid password");
					}
				}else{
					unset($to_update_fields['new_password']); unset($to_remove_fields['new_password']);
					unset($to_update_fields['password']); unset($to_remove_fields['new_password']);
				}
			}
			
			$collection = self::get_collection(self::$collection);
			$operations['$set'] = $to_update_fields;
			if(!empty($to_remove_fields)){
				$operations['$unset'] = $to_remove_fields;
			}
			$result = $collection->updateOne(
				['_id'=> new \MongoDB\BSON\ObjectId($this->id)],
				$operations
			);
			
			if($result->getModifiedCount() > 0){
				// Update comments
				if(isset($to_update_fields['avatar']) && isset($to_update_fields['display_name'])){
					if($to_update_fields['avatar'] != $this->avatar || $to_update_fields['display_name'] != $this->display_name)
					Comment::update_user($this->id, $to_update_fields['avatar'], $to_update_fields['display_name']);
				}
			}
			
			return $result->getMatchedCount() > 0 ? true  : false ; 
		}
	}
	
	public function put_group($role){
		if(!in_array($role, [self::GROUP_USER, self::GROUP_MOD, self::GROUP_ADMIN, self::GROUP_SENIOR_ADMIN, self::GROUP_FULL_ADMIN])){
			throw new \Exception("Invalid role assignment!");
		}
		return $this->update(['group'=>$role]);
	
	}
	
	public function get_group(){
		return 
			$this->group;
	}
	
	public function unban(){
		$collection = self::get_collection($collection);
		
		$collection->updateOne(['_id'=>new \MongoDB\BSON\ObjectId($this->id)],[
			'$unset'=>[
				'banned' => 1
			]
		]);
		
		return $result->getMatchedCount() > 0 ? true : false ;
	}
	
	public static function user_exists($id, $account_type = self::ACCOUNT_TYPE_LOCAL){
		
		if($account_type == self::ACCOUNT_TYPE_LOCAL){
			$email = $id;
			$collection = self::get_collection(self::$collection);
			return $collection->findOne([
				'email'=>$email, 
				'account_type'=> self::ACCOUNT_TYPE_LOCAL
			]) ? true : false ;	
		}
		
		if($account_type == self::ACCOUNT_TYPE_FB){
			return $collection->findOne(['fb_id'=>$id])['fb_id'] ? true : false ;
		}
		
		if($account_type == self::ACCOUNT_TYPE_GOOG){
			return $collection->findOne(['goog_id'=>$id])['goog_id'] ? true : false ; 
		}
	}
	
	public static function authenticate($email, $password){
		if($user = self::find_by_email($email, self::ACCOUNT_TYPE_LOCAL)){
			if(password_verify($password, $user->password)){
				return self::instantiate($user);
			}else{
				return false;
			}
		}
	}
		
	private static function find_by_email($email){
		if($user = self::get_collection(self::$collection)->findOne(['email'=>$email, 'account_type'=> self::ACCOUNT_TYPE_LOCAL])){
			$user = self::instantiate($user);	
			return $user;
		}
	}
	
	private function validate_fields($fields=[]){
		$whitelisted_fields = get_object_vars($this);
		// Remove unnecessary fields
		unset($whitelisted_fields['collection']);
		// Get fields keys
		$whitelisted_fields = array_keys($whitelisted_fields);
		$is_valid = false;
		// Whitelist all fields
		foreach($fields as $x=>$y){
			if(!in_array($x, $whitelisted_fields)){
				$is_valid = false;
				throw new \Exception("Not valid field '$x'");
				break;
			}else{
				$is_valid = true ;
			}
		}
		
		// Validate form
		if(isset($fields['name'])){
			// Validate last name
			if(strlen($fields['name']) < 3){
				throw new \Exception("Name should be atleast 3 characters");
				return false;
			}
		}
		if(isset($fields['email'])){
			// Validate last name
			if(!filter_var($fields['email'], FILTER_VALIDATE_EMAIL)){
				throw new \Exception("Invalid email");
				return false;
			}
		}
		if(isset($fields['password'])){
			// Validate last name
			if(strlen($fields['password']) < 6 || empty($fields['password'])){
				throw new \Exception("Password should be atleast 6 characters");
				return false;
			}
		}
		if(isset($fields['city'])){
			// Validate last name
			if(strlen($fields['city']) < 3){
				throw new \Exception("City should be atleast 3 characters");
				return false;
			}
		}
		if(isset($fields['country'])){
			// Validate last name
			if(empty($fields['country'])){
				throw new \Exception("Country can not be blank");
				return false;
			}
		}
		if(isset($fields['gender'])){
			if($fields['gender'] != '0' && $fields['gender'] != '1' && $fields['gender'] != 'undefined'){
				throw new \Exception("Invalid gender");
				return false;
			}
		}
		if(isset($fields['dob'])){
			// Validate dob
			$day = $fields['dob']['day'];
			$month = $fields['dob']['month'];
			$year = $fields['dob']['year'];
			$min_year = date('Y',time()) - 13; 
			if(!isset($day) || !isset($month) || !isset($year) || $day > 31 || $month > 12 || !is_numeric($year)){
				throw new \Exception("Invalid date");
				return false;
			}
			if($year > $min_year){
				throw new \Exception("Minimum age required is 13");
			}
		}
		if(isset($fields['type'])){
			// Validate Type
			if(
				$fields['type'] != self::ACCOUNT_TYPE_LOCAL || 
				$fields['type'] != self::ACCOUNT_TYPE_FB ||
				$fields['type'] != self::ACCOUNT_TYPE_GOOG
			){
				throw new \Exception("Invalid account type");
				return false;
			}
		}
		
		return $is_valid;
	}
	
	public static function record_good_report($user_id, $report_id){
		$collection = self::get_collection(self::$collection);
		
		if(!ctype_xdigit($user_id) && !ctype_xdigit($report_id)){
			throw new \Exception("Invalid user or report ID");
		}
		
		$update = $collection->updateOne(
			['_id'=> new \MongoDB\BSON\ObjectId($user_id)],
			[
				'$addToSet'=>[
					'stats.success_reports'=>$report_id
				]
			]
		);
		return $update->getMatchedCount() > 0 ? true : false ;
	} 
	
	public static function record_bad_report($user_id, $report_id){
		$collection = self::get_collection(self::$collection);
		
		if(!ctype_xdigit($user_id) && !ctype_xdigit($report_id)){
			throw new \Exception("Invalid user or report ID");
		}
		
		$update = $collection->updateOne(
			['_id'=> new \MongoDB\BSON\ObjectId($user_id)],
			[
				'$addToSet'=>[
					'stats.false_reports'=>$report_id
				]
			]
		);
		return $update->getMatchedCount() > 0 ? true : false ;
	} 
	
	public static function get_fb_token($token){
		$url = "https://graph.facebook.com/v2.12/oauth/access_token?grant_type=fb_exchange_token&client_id=".Config::FB_APP_ID."&client_secret=".Config::FB_APP_SECRET."&fb_exchange_token=$token";
		if($res = file_get_contents($url)){
			$graphNode = json_decode($res, true);
			return $graphNode['access_token'];	
		}else{
			return false;
		}
	}
	
	public static function generate_name($first, $second){
		$x =[
			['bottle', 'sky', 'silver', 'poppin', 'screamin', 'underexposed', 'shallow', 'sensitive', 'noisy', 'sharp', 'blown', 'soft', 'raw', 'macro', 'cold', 'blurry', 'fantasia', 'chesty', 'star', 'diamond', 'montana', 'sugar', 'mimi', 'lola', 'kitty'],
			['polroid', 'hot shoe', 'flash', 'rocket', 'flare', 'spider', 'cat', 'explosion', 'flyer', 'hooter', 'horn', 'thighs', 'hips', 'jugs', 'kiss', 'sizzle', 'cream', 'glitz', 'lick', 'juice']
		];
		echo $x[0][rand(0,24)] . " " . $x[1][rand(0,19)];
	}
	
	protected static function instantiate($result){ 
		$obj = new self;
		foreach($result as $k=>$v){
			if($k == "_id" || $k == "id"){
				$k = "id"; $v = (string) $v;
			}
			$obj->$k = $v;
		}
		return $obj;
	}

}

?>